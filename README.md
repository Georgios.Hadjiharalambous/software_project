Web application for a cava store, under the theory of crowdsourcing.<br/>
The webpage is made to be responsive using angular.<br/>
Frontend made using angular. <br/>
Backend using Java and Hibernate framework.<br/>
For the database both MariaDb and mySQL were tested.
A small video tutorial of the application can be found [here](https://www.youtube.com/watch?v=Re6CJ6ruKKU&t=87s).

# Software Engineering Project

## Members

Name |username
---  | ---
Γεώργιος Χατζηχαραλάμπους   |   @Georgios.Hadjiharalambous
Μιχάλης Παπαδόπουλλος       |   @xmpf
Χαράλαμπος Κάρδαρης         |   @ckardaris
Ελευθερία Μαππούρα          |   @Eleftheria
Αλέξανδρος Θεοδώρου         |   @AfroNinja
