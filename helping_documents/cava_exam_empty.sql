-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 04, 2019 at 11:48 AM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cava_exam_empty`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_expired_token` ()  NO SQL
Delete from userTokens where token_id in
(select token_id from (Select token_id from userTokens
where (DATE_SUB(creation_date, INTERVAL -30 DAY) < NOW())) as c)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `drop_temp` ()  NO SQL
DROP  TEMPORARY TABLE temp$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `drop_temp1` ()  NO SQL
DROP  TEMPORARY TABLE temp1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `drop_temp2` ()  NO SQL
Drop TEMPORARY table temp2$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `drop_temp3` ()  NO SQL
DROP  TEMPORARY TABLE temp3$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `drop_temp5` ()  NO SQL
DROP  TEMPORARY TABLE temp5$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `drop_temp6` ()  NO SQL
DROP  TEMPORARY TABLE temp6$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `geodist` ()  NO SQL
CREATE TEMPORARY table if not exists temp1 
(
SELECT t.*,0 as distance From temp as t
    )$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `geodist2` (IN `lng` DOUBLE, IN `lat` DOUBLE, IN `dist` INT)  NO SQL
CREATE TEMPORARY table if not exists temp1(
SELECT t.*, ( 6371 * acos( cos( radians(lat) ) 
              * cos( radians( dest.lat ) ) 
              * cos( radians(dest.lng ) - radians(lng) ) 
              + sin( radians(lat) ) 
              * sin( radians( dest.lat ) ) ) )as distance FROM shops as dest,temp as t where dest.shop_id=t.shopId  having distance < dist 
    )$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `geodist3` (IN `lng` DOUBLE, IN `lat` DOUBLE, IN `dist` INT)  NO SQL
CREATE TEMPORARY table if not exists temp3(
SELECT t.*, ( 6371 * acos( cos( radians(lat) ) 
              * cos( radians( dest.lat ) ) 
              * cos( radians(dest.lng ) - radians(lng) ) 
              + sin( radians(lat) ) 
              * sin( radians( dest.lat ) ) ) ) as distance FROM shops as dest,temp2 as t where dest.shop_id=t.shop_id  having distance < dist 
    )$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `geodist4` (IN `lng` DOUBLE, IN `lat` DOUBLE, IN `dist` INT)  NO SQL
CREATE TEMPORARY table if not exists temp6(
SELECT t.*,( 6371 * acos( cos( radians(lat) ) 
              * cos( radians( dest.lat ) ) 
              * cos( radians(dest.lng ) - radians(lng) ) 
              + sin( radians(lat) ) 
              * sin( radians( dest.lat ) ) ) ) as distance FROM shops as dest,temp5 as t where dest.shop_id=t.shopId  having distance < dist 
    )$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `geodist5` ()  NO SQL
CREATE TEMPORARY table if not exists temp6 
(
SELECT t.*,0 as distance From temp5 as t
    )$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `user_id` bigint(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`user_id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE `price` (
  `price_id` bigint(60) NOT NULL,
  `price` double NOT NULL,
  `date` date NOT NULL,
  `shop_id` bigint(60) NOT NULL,
  `product_id` bigint(60) NOT NULL,
  `user_id` bigint(60) NOT NULL,
  `price_verified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `price`
--
DELIMITER $$
CREATE TRIGGER `mpla` BEFORE UPDATE ON `price` FOR EACH ROW if (Old.price<>New.price and old.price_id=new.price_id) then begin
	INSERT into old_prices VALUES(old.price,old.user_id,old.product_id,old.shop_id) ;
    end;
end if
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` bigint(60) NOT NULL,
  `name` varchar(45) NOT NULL,
  `alcohol` double DEFAULT NULL,
  `quantity_ml` int(11) NOT NULL,
  `description` varchar(150) DEFAULT NULL,
  `category` varchar(45) NOT NULL,
  `tags` text NOT NULL,
  `withdrawn` tinyint(1) NOT NULL DEFAULT '0',
  `barcode_id` bigint(20) DEFAULT '0',
  `likes` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `products`
--
DELIMITER $$
CREATE TRIGGER `product_id_deletion` BEFORE DELETE ON `products` FOR EACH ROW BEGIN
DELETE FROM price WHERE price.product_id=Old.product_id;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `shop_id` bigint(60) NOT NULL,
  `name` varchar(450) NOT NULL,
  `address` varchar(450) NOT NULL,
  `telephone_number` int(11) DEFAULT NULL,
  `lng` double NOT NULL,
  `lat` double NOT NULL,
  `tags` text NOT NULL,
  `withdrawn` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `shops`
--
DELIMITER $$
CREATE TRIGGER `shop_id_deletion` BEFORE DELETE ON `shops` FOR EACH ROW BEGIN
DELETE FROM price WHERE price.shop_id=Old.shop_id;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` bigint(60) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(80) NOT NULL,
  `reputation` float NOT NULL,
  `age` int(11) NOT NULL,
  `date_of_registration` date NOT NULL,
  `sex` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `surname`, `username`, `password`, `email`, `reputation`, `age`, `date_of_registration`, `sex`) VALUES
(1, 'root', 'root', 'root', 'e21fc56c1a272b63ed143979d598cf8b8329', 'jg@gmal.com', 1, 1, '2019-03-18', 1),
(2, 'gxxshumi', 'Hadjiharalambous', 'gxx', 'gxx', 'gxxshumi@gmail.com', 10, 10, '2019-03-12', 1);

--
-- Triggers `users`
--
DELIMITER $$
CREATE TRIGGER `user_id_deletion` BEFORE DELETE ON `users` FOR EACH ROW BEGIN
DELETE FROM price WHERE price.user_id=Old.user_id;
DELETE FROM admins WHERE admins.user_id=Old.user_id;
DELETE from userTokens WHERE userTokens.username=Old.username;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `userTokens`
--

CREATE TABLE `userTokens` (
  `token_id` bigint(60) NOT NULL,
  `username` varchar(50) NOT NULL,
  `token` varchar(300) NOT NULL,
  `creation_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`price_id`),
  ADD KEY `FK_182` (`product_id`),
  ADD KEY `FK_1882` (`shop_id`),
  ADD KEY `FK_1088` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`shop_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `userTokens`
--
ALTER TABLE `userTokens`
  ADD PRIMARY KEY (`token_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `price`
--
ALTER TABLE `price`
  MODIFY `price_id` bigint(60) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` bigint(60) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `shop_id` bigint(60) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` bigint(60) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `userTokens`
--
ALTER TABLE `userTokens`
  MODIFY `token_id` bigint(60) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `FK_100` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `price`
--
ALTER TABLE `price`
  ADD CONSTRAINT `FK_1088` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `FK_14` FOREIGN KEY (`shop_id`) REFERENCES `shops` (`shop_id`),
  ADD CONSTRAINT `FK_18` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
