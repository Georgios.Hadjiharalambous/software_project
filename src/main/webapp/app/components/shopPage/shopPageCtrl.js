var shopPageCtrl = function($scope, $http, $location, $routeParams){
	$scope.shop = null;
	$scope.searchId = null;

	$scope.image = "assets/img/cava.jpg"
	if($location.path().indexOf("/shop/") == 0){
		
		$scope.searchId = $routeParams["id"];

		ids=[];
		ids.push($scope.searchId)
		
		var url= "api/shops/" + $scope.searchId;
		var config = {
			responseType: "json",
		}	
		$http.get(url, config).then(
			function success(response){
				$scope.shop = response.data;
				document.title = "HoS - " + $scope.shop.name;
			},
			function fail(response){
				//console.log("Failed to retrieve resource from server");
				var error = "status=" + response.status
				if(response.statusText)
					error += "&text=" + response.statusText;
				$location.url("error?" + error)
			}
		);
	}

	$scope.showMap = function(){
		$("#mapModal").modal("show");
	}

	$scope.cancel = function () {
		$("#mapModal").modal("hide");
  	};
};
