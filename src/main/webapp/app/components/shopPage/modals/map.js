var myStyles=[
  {
    featureType: "administrative.land_parcel",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "administrative.neighborhood",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "poi",
    elementType: "labels.text",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "poi.business",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "poi.park",
    elementType: "labels.text",
    stylers: [
      {
        "visibility": "off"
      }
    ]
  },
  {
    featureType: "road",
    elementType: "labels",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "labels.text",
    stylers: [
      {
        visibility: "off"
      }
    ]
  }
];


          //Angular App Module and Controller
          var mapApp = angular.module('mapApp', []);
          mapApp.controller('MapController', function ($scope, $http) {
        var cities = [];
		 
              var mapOptions = {
                  zoom: 10,
                  center: new google.maps.LatLng(37.983810,23.727539),
                  mapTypeId: google.maps.MapTypeId.ROADMAP,
                  styles: myStyles
              }
            
              $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
 
              $scope.markers = [];
              
              var infoWindow = new google.maps.InfoWindow();
              
              var createMarker = function (info){
                  
                  var marker = new google.maps.Marker({
                      map: $scope.map,
                      position: new google.maps.LatLng(info.lat, info.long),
                      title: info.place
                  });

                  marker.content = '<div class="infoWindowContent">' + info.desc + '<br />' + info.lat + ' E,' + info.long +  ' N, </div>';
                  
                  google.maps.event.addListener(marker, 'click', function(){
                      infoWindow.setContent('<h2>' + marker.title + '</h2>' + 
                        marker.content);
                      infoWindow.open($scope.map, marker);
                  });
                  console.log(marker)
                  $scope.markers.push(marker);
                  
              }
              

			    var  ids= window.parent.ids
				for (i = 0; i < ids.length; i++){
					var url = "../../../../api/shops/"+ ids[i];
					var config = {
						responseType: "json"
					};
					var shop = null
					$http.get(url, config).then(
						function success(response){
							shop  = response.data;
							cities.push({
								place: shop.name,
								desc: shop.address,
								lat: shop.lat,
								long: shop.lng
							})
							for (var i of  cities){
								createMarker(i);
						  	}
						},
						function fail(response){
							console.log("Failed to retrieve shop resource");
							console.log(response)
						}
					);
				}

          });
