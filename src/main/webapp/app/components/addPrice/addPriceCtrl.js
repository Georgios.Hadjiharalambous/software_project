var addPriceCtrl = function($scope, $http, $location, $filter, $routeParams, $rootScope, $httpParamSerializer){
	//var url = "app/components/addPrice/prices.json";
	$scope.today = new Date();
	$scope.today = $filter('date')($scope.today,"yyyy-MM-dd");
	document.title = "HoS - Προσθήκη Τιμής"
	
	$scope.newPrice = {
		productId : null,
		productName : null,
		barcode : null,
		productName : null,
		shopAddress : null,
		shopName : null,
		price : null,
		dateFrom : null,
		dateTo : null
	}

	$scope.shops = []
	$scope.names = []
	var getNames = function(){
		var url = "api/shops"
		var config = {
			responseType : "json"
		}
		var total = null
		$http.get(url,config).then(
			function success(response){
				total = response.data.total;
				url+="?count=" + total
				//console.log(url)
				$http.get(url,config).then(
					function success(response){
						for (shop of response.data.shops){
							var object = {
								name : shop.name,
								id : shop.id
							}
							$scope.shops.push(object)
							for (shop of $scope.shops)
								$scope.names.push(shop.name)
						}
						/*
						var onlyUnique = function (value, index, self) {
							return self.indexOf(value) === index;
						}
						$scope.shops = $scope.shops.filter( onlyUnique );
						$scope.shops = [...new Set($scope.shops.map(item => item.name))];
						//console.log($scope.shops)
						*/
					},
					function error(response){
						//console.log(response);
						var error = "status=" + response.status + "&text=" + response.statusText;
						$location.url("error?" + error)
					}
				)
			},
			function error(response){
				//console.log(response);
				var error = "status=" + response.status
				if(response.statusText)
				 	error += "&text=" + response.statusText;
				$location.url("error?" + error)
			}
		)
	}

	getNames()

	$scope.products = []
	$scope.pNames = []
	var getpNames = function(){
		var url = "api/products"
		var config = {
			responseType : "json"
		}
		var total = null
		$http.get(url,config).then(
			function success(response){
				total = response.data.total;
				url+="?count=" + total
				//console.log(url)
				$http.get(url,config).then(
					function success(response){
						for (product of response.data.products){
							var object = {
								name : product.name,
								id : product.id
							}
							$scope.products.push(object)
							for (product of $scope.products)
								$scope.pNames.push(product.name)
						}
						/*
						var onlyUnique = function (value, index, self) {
							return self.indexOf(value) === index;
						}
						$scope.products = $scope.products.filter( onlyUnique );
						$scope.products = [...new Set($scope.products.map(item => item.name))];
						//console.log($scope.products)
						*/
					},
					function error(response){
						//console.log(response);
						var error = "status=" + response.status
						if(response.statusText)
							error += "&text=" + response.statusText;
						$location.url("error?" + error)
					}
				)
			},
			function error(response){
				//console.log(response.data);
				var error = "status=" + response.status
				if(response.statusText)
				 	error += "&text=" + response.statusText;
				$location.url("error?" + error)
			}
		)
	}

	getpNames()
	
	$scope.nameError = false; 
	$scope.checkName = function(){
		if ($scope.names.indexOf($scope.newPrice.shopName) != -1)
			$scope.nameError = false;
		else 
			$scope.nameError = true;
	}
	$scope.checkpName = function(){
		if ($scope.pNames.indexOf($scope.newPrice.productName) != -1)
			$scope.pNameError = false;
		else 
			$scope.pNameError = true;
	}
	if($location.path().indexOf("/addPrice") == 0){
		if($routeParams["product"]){
			$scope.newPrice.productId = $routeParams["product"];
			var url="api/products/" + $scope.newPrice.productId;
			var config = {
				responseType : "json"
			}
			$http.get(url,config).then(
				function success(response){
					$scope.newPrice.productName = response.data.name;
					$scope.newPrice.barcode = response.data.extraData.barcode_id;
				},
				function error(response){
					//console.log(response.data);
					var error = "status=" + response.status
					if(response.statusText)
						error += "&text=" + response.statusText;
					$location.url("error?" + error)
				}
			)
		}
		if($routeParams["shop"]){
			$scope.newPrice.shopId = $routeParams["shop"];
			var url="api/shops/" + $scope.newPrice.shopId;
			var config = {
				responseType : "json"
			}
			$http.get(url,config).then(
				function success(response){
					$scope.newPrice.shopName = response.data.name;
				},
				function error(response){
					//console.log(response.data);
					var error = "status=" + response.status
					if(response.statusText)
						error += "&text=" + response.statusText;
					$location.url("error?" + error)
				}
			)
		}
	}

	$scope.barcodeFound = false
	$scope.barcodeError = false;
	$scope.disable = {
		scan: false,
		message: true
	}
	ScanditSDK.configure("AdN8nSDjPA5+RD4fsw0/i5QIhIDaNRp3IVcfL4QLE8uocZTkpxzsCvMB3s/uX6bwgUty4qhJk88MVhMcfFHp0VouuWt1WRL07HjApONTKaYRQQtVYl2+1pkCWbpBRhHYLjMzJDp0iJ58Owpxu7ysGoOZK0GxIZMroQkE/Nslq94c1w9HLyM/wsjVEmhv/9nB41NXjemGF8m5wJakKnJh+ykNieiV9kTErU5nA0L6MtyMWTqubfO5bcMIds0AnV8GqPETBHjAC9bb+rekHDGhkrQJ1ZKBoQjESeUV9H1JEzC8Y1YamJZZfwUCpTVJFic4Bphn9f0hTWwRdsFwGEhkuRj3KVuIbn0CnfypfM/7QpZa6CbsELHSoxpuz8NkwnMax1SjNZ8P3GDLGgfWblilG3Kma3HKd0y7UbRU70Xf6QmHP2f/9rfRJNW9qii0MzNHDBjXYwH/6xhQ4+QhHaRb2Y2No+Q4q/wvnhEuDd4hDufUh1994g5J98zmdfBiQwV9aVCo981xJ9qyEix4HW7mH2KERzPduzb5FCbs5fsAuP9JG6LXhBhDSM5r2yDVu3b+sGLTYxMPv3FO2IwJx021HDcmplL4szFQDh0sVoayxWaz2/J4sBXrVh7IoUjVje9t9v0w3k106McYjyZ401pUcqKCmATl/FckOaxn7aDikiMSA5wTa6WXUzICDjmRwgJoHu78bbKP5kJV7gXXiP2zd0u+WB1bsoDEkfPD772OQlWbxhvjk//3To9UXgYnAjaPdUYxlasTOn5RiEa4VDzzFZ/52q9bXDFmpBHbNbTmoC0EuA==", {
		engineLocation: "https://cdn.jsdelivr.net/npm/scandit-sdk/build",
	})
	
	var end = null;
	$scope.scan = function(){
		$scope.disable.scan = true;
		picker = ScanditSDK.BarcodePicker;
		picker.create(document.getElementById("scandit-barcode-picker"), {
			playSoundOnScan: false,
			vibrateOnScan: true
		}).then(function(barcodePicker) {
			// barcodePicker is ready here to be used
			var scanSettings = new ScanditSDK.ScanSettings({
				enabledSymbologies: ["ean8", "ean13"],
				codeDuplicateFilter: 1000
			});
			end = barcodePicker;
			barcodePicker.applyScanSettings(scanSettings);
			barcodePicker.onScan(function(scanResult) {
				scanResult.barcodes.reduce(function(string, barcode) {
					$scope.$apply(function(){
						$scope.newPrice.barcode = Number(barcode.data);
						$scope.disable.bar = true;
						$scope.barcodeQuery();
						
					});
				}, "");
				this.destroy();
				$scope.$apply( 
					function(){
						$scope.disable.scan = false;
					}
				);
			});
		}
		);
	}
	$scope.reset = function(){
		$scope.newPrice = {
			productId : null,
			barcode : null,
			productName : null,
			shopId : null,
			shopName : null,
			price : null,
			dateFrom : null,
			dateTo : null,
		}
		$scope.disable = {
			scan: false,
			message: true
		}
	}

	$scope.barcodeQuery = function(){
		if($scope.newPrice.barcode != ""){
			$scope.disable.message = false;
			var url = "ProductController";
			var config = {
				response: "json",
				params: {
					action: "barcodes",
					barcodes: [$scope.newPrice.barcode]
				}
			}
			$http.get(url,config).then(
				function success(response){
					if(response.data.total == 1){
						/*
						$scope.newPrice = {
							//found : true,
							productId: response.data.products[0].id,
							productName: response.data.products[0].name,
						}
						*/
						$scope.barcodeFound = true
						$scope.newPrice.productId = response.data.products[0].id
						$scope.newPrice.productName = response.data.products[0].name
						//$scope.reset(); 
						//I won't be needing the form data again.
						//I visit price page or add new Price
						//console.log(response.data)
					}	
					$scope.modal("barcodeQueryModal", "show");
					//console.log($scope.newPrice)
				},
				function error(response){
					$scope.barcodeError = true;
					$scope.modal("barcodeQueryModal", "show");
				}
			)
		}
	}

	$scope.modal = function(name, type){
		if(type == "show")
			$("#"+ name).modal("show");
		else if(type == "hide")
			$("#"+ name).modal("hide");
	}
	$scope.goToAddProduct = function(name, bar){
		$("#" + name).modal("hide");
		$("#" + name).on("hidden.bs.modal", function () {
			$scope.$apply(function(){
				$location.url("/addProduct?barcode=" + bar);
			});
		});
	}
	$scope.goToProduct = function(name, id){
		$("#" + name).modal("hide");
		$("#" + name).on("hidden.bs.modal", function () {
			$scope.$apply(function(){
				$location.url("/product/" + id);
			});
		});
	}

	$scope.goToMain = function(){
		$("#barcodeQueryModal").modal("hide");
		$("#barcodeQueryModal").on("hidden.bs.modal", function () {
			$scope.$apply(function(){
				$location.url("/mainPage");
			});
		});
	}

	$scope.cancel = function(){
		if(end !== null) end.destroy();
		else $("#scandit-barcode-picker").html("");
		$scope.disable.scan = false;
	}
	$scope.coordinates = {
		longitude: null,
		latitude: null
	}
	$scope.error = {
		show : false,
		GPS: null
	};
	$scope.closeShops = {
		names : null,
		ids : null
	}

	$scope.submit = function(){
		var url = "api/prices";
		for (shop of $scope.shops)
			if ($scope.newPrice.shopName == shop.name)
				$scope.newPrice.shopId = shop.id
		for (product of $scope.products)
			if ($scope.newPrice.productName == product.name)
				$scope.newPrice.productId = product.id
		var df = null;
		var dt = null;
		if ($scope.newPrice.dateFrom)
			df = $filter('date')($scope.newPrice.dateFrom,"yyyy-MM-dd")
		else
			df = $scope.today
		if ($scope.newPrice.dateTo)
			dt = $filter('date')($scope.newPrice.dateFrom,"yyyy-MM-dd")
		else
			dt = $scope.today
		var data = {
			price : $scope.newPrice.price,
			dateFrom : df,
			dateTo : dt,
			productId : $scope.newPrice.productId,
			shopId : $scope.newPrice.shopId,
		}
		var config = {
			headers : {
				"X-OBSERVATORY-AUTH" : $rootScope.token,
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			//responseType: "json"
		}
		data = $httpParamSerializer(data);
		//console.log(data)
		$http.post(url, data, config).then(
			function success(response){
				//$scope.newPrice.id = response.data.id;
				$scope.modal("addSuccessModal", "show");
			},
			function error(response){
				var error = "status=" + response.status
				if(response.statusText)
					error += "&text=" + response.statusText;
				//console.log(response.data);
				$location.url("error?" + error)
			}
		);
	};

	$scope.addShop = function(){
		$location.url("addShop")
	}
};

