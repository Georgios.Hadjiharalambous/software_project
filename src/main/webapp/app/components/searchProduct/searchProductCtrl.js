var help2 = null;
var address2;
var longitude2;
var latitude2;

var searchProductCtrl = function($location, $scope, $rootScope, $http, $routeParams, $filter){
	
	document.title = "HoS - Αναζήτηση"
		
	var today = new Date();
  	today = $filter('date')(today,"yyyy-MM-dd");

	$scope.params = {
		page: 1,
		start: 0,
		count: 20,
		dateFrom: null,
		dateTo: null,
		geoDist: null,
		geoLng: null,
		geoLat: null,
		sort: "price|ASC",
		tags: null,
		min : null,
		max :null,
		address : null
	}


	$scope.filter = {
		beer: null,
		wine: null,
		whiskey: null,
		gin: null,
		vodka: null,
		other: null,

		minprice: 0, 
		maxprice: 2000,

		minradius:null,
		maxradius : 10000 ,

		location: null,
		dateFrom: null,
		dateto: null
		
	};

	$scope.geo = {
		lng: null,
		lat: null,
	}
	
	$scope.beer = "";
	$scope.lager = "";
	$scope.ale = "";
	$scope.lambic = "";
	$scope.origin = "";
	$scope.ingredients = "";
	$scope.american = "";
	$scope.colour = "";
	$scope.sugar = "";
	$scope.carbon = "";

	$scope.newUrl = function(){		
		var url = "/search?"

		if($scope.params.page && $scope.params.page != "1")
			url += "page=" + $scope.params.page + "&";
		/*
		if($scope.params.page)
			$scope.params.start = ($scope.params.page - 1) * $scope.params.count;
		if($scope.params.start!=0)
			url += "start=" + $scope.params.start + "&";
		*/
		if($scope.params.count!=20)
			url += "count=" + $scope.params.count + "&";
		if($scope.params.min)
			url += "min=" + $scope.params.min + "&";
		if($scope.params.max)
			url += "max=" + $scope.params.max + "&";
		/*
		if($scope.params.status!="ACTIVE")
			url += "status=" + $scope.params.status + "&";
		*/
		if($scope.params.sort!="price|ASC"){
			if (Array.isArray($scope.params.sort))
				for( let i in $scope.params.sort){
					url += "sort=" + $scope.params.sort[i] + "&";
				}
			else
				url += "sort=" + $scope.params.sort + "&";
		}
		if($scope.params.tags){
			if (Array.isArray($scope.params.tags))
				for( let i in $scope.params.tags){
					url += "tags=" + $scope.params.tags[i] + "&";
				}
			else
				url += "tags=" + $scope.params.tags + "&";
		}
		if($scope.params.geoDist)
			url += "geoDist=" + $scope.params.geoDist + "&";
		if($scope.params.geoLng)
			url += "geoLng=" + $scope.params.geoLng + "&";
		if($scope.params.geoLat)
			url += "geoLat=" + $scope.params.geoLat + "&";
		if ($scope.params.dateFrom)
			url += "dateFrom=" + $scope.params.dateFrom + "&";
		else
			url += "dateFrom=" + today + "&";
		if($scope.params.dateTo)
			url += "dateTo=" + $scope.params.dateTo + "&";
		else
			url += "dateTo=" + today + "&";
		if($scope.params.address)
			url += "address=" + $scope.params.address + "&";

		return url;
	}

	$scope.changeSort = function(type, dir){
		$scope.params.sort = type + "|" + dir;
		$location.url($scope.newUrl());	
	}
	
	$scope.changePage = function(page){
		//page I want to go starting at 1
		if (page < $scope.lastPage + 1 && page > 0){
			$scope.params.page = page;
			$location.url($scope.newUrl());	
		}
	}

	if($location.path().indexOf("/search") == 0){

		if($routeParams["page"])
			$scope.params.page = $routeParams["page"];
		if($routeParams["count"])
			$scope.params.count = $routeParams["count"];
		if($routeParams["sort"])
			$scope.params.sort = $routeParams["sort"];
		if($routeParams["min"]){
			$scope.params.min = $routeParams["min"];
			$scope.filter.minprice = parseInt($routeParams["min"]);
		}
		if($routeParams["max"]){
			$scope.params.max = $routeParams["max"];
			$scope.filter.maxprice = parseInt($routeParams["max"]);
		}
		if($routeParams["geoDist"]){
			$scope.params.geoDist = $routeParams["geoDist"];
			$scope.filter.maxradius =  parseInt($routeParams["geoDist"]);
		}
		if($routeParams["geoLng"])
			$scope.params.geoLng = $routeParams["geoLng"];
		if($routeParams["geoLat"])
			$scope.params.geoLat = $routeParams["geoLat"];
		if($routeParams["tags"]){
			$scope.params.tags = $routeParams["tags"];
			if (Array.isArray($scope.params.tags))
				for( let i in $scope.params.tags){
					url += "tags=" + $scope.params.tags[i] + "&";
				}
			else
				url += "tags=" + $scope.params.tags + "&";
		}
		if($routeParams["dateFrom"]){
			$scope.params.dateFrom = $routeParams["dateFrom"];
			$scope.filter.dateFrom = new Date($scope.params.dateFrom);
		}	
		if($routeParams["dateTo"]){
			$scope.params.dateTo = $routeParams["dateTo"];
			$scope.filter.dateTo = new Date($scope.params.dateTo);
		}
		if($routeParams["address"]){
			$scope.params.address = $routeParams["address"];
			$scope.filter.location = $scope.params.address;
		}


		var url="api/prices?";
		if($scope.params.page && $scope.params.page != "1")
			url += "start=" + ($scope.params.page - 1) * $scope.params.count + "&";
		if($scope.params.count!=20)
			url += "count=" + $scope.params.count + "&";
		if($scope.params.sort!="price|ASC"){
			if (Array.isArray($scope.params.sort))
				for( let i in $scope.params.sort){
					url += "sort=" + $scope.params.sort[i] + "&";
				}
			else
				url += "sort=" + $scope.params.sort + "&";
		}
		if($scope.params.tags){
			if (Array.isArray($scope.params.tags))
				for( let i in $scope.params.tags){
					url += "tags=" + $scope.params.tags[i] + "&";
				}
			else
				url += "tags=" + $scope.params.tags + "&";
		}
		if($scope.params.geoDist)
			url += "geoDist=" + $scope.params.geoDist + "&";
		if($scope.params.geoLng)
			url += "geoLng=" + $scope.params.geoLng + "&";
		if($scope.params.geoLat)
			url += "geoLat=" + $scope.params.geoLat + "&";
		if ($scope.params.dateFrom)
			url += "dateFrom=" + $scope.params.dateFrom + "&";
		else
			url += "dateFrom=" + today + "&";
		if($scope.params.dateTo)
			url += "dateTo=" + $scope.params.dateTo + "&";
		else
			url += "dateTo=" + today + "&";
		////console.log(url)
		var config = {
			responseType: "json"
		}
		$http.get(url, config).then(
			function success(response){
				
				$scope.params.start = response.data.start;
				$scope.params.count = response.data.count;
				$scope.params.total = response.data.total;
				$scope.params.page = $scope.params.start / $scope.params.count + 1;
				$scope.lastPage = Math.floor(response.data.total / $scope.params.count) + 1;
				var all = response.data.prices;
				$scope.prices = [];
				for (price of all){
					if( ($scope.params.min && price.price < $scope.params.min) || ($scope.params.max && price.price > $scope.params.max) )
						continue;
					$scope.prices.push(price);
				}
				var temp = new Set();
				for (price of $scope.prices){
					temp.add(price.shopId)
				}
				ids = [...temp]
				////console.log(ids)
			},
			function fail(response){
   				var error = "status=" + response.status
    			if(response.statusText)
     				error += "&text=" + response.statusText;
				$location.url("error?" + error)
				////console.log(response)
				////console.log("Failed to retrieve price resources");
			}
		);
	}
	$scope.showMap = function(){
		$("#mapModal").modal("show");
	}

	$scope.cancel = function () {
		$("#mapModal").modal("hide");
  	};

	//assign values
	$scope.checkbeer = function(name) {
		$scope.filter.beer = name;
		//////console.log($scope.filter.beer);
	};

	//assign values
	$scope.checkwine = function(name) {
		$scope.filter.wine = name;
		//////console.log($scope.filter.wine);
	};
	
	//assign values
	$scope.checkwhiskey = function(name) {
		$scope.filter.whiskey = name;
		//////console.log($scope.filter.whiskey);
	};
	
	//assign values
	$scope.checkgin = function(name) {
		$scope.filter.gin = name;
		//////console.log($scope.filter.gin);
	};
	
	//assign values
	$scope.checkvodka = function(name) {
		$scope.filter.vodka = name;
		//////console.log($scope.filter.vodka);
	};

	//assign values
	$scope.checkother = function(name) {
		$scope.filter.other = name;
		//////console.log($scope.filter.other);
	};

	//assign values
	$scope.checkprice = function(name1,name2) {
		if(name1 == 'price20') { $scope.filter.maxprice = 20; $scope.filter.minprice = name2;}
		else if(name1 == 'price40')	{ $scope.filter.maxprice = 40; $scope.filter.minprice = name2;}
		else { $scope.filter.maxprice = name1; $scope.filter.minprice = name2;}
		//////console.log($scope.filter.maxprice);
		//////console.log($scope.filter.minprice);
	};

	//assign values
	$scope.checkradius = function(name1,name2) {
		////console.log(name1);
		////console.log(name2);
		if (name1 == 'radius250') { $scope.filter.maxradius = 250; $scope.filter.minradius = 0;}
		else if(name1 == 'radius500')	{ $scope.filter.maxradius = 500; $scope.filter.minradius = 0;}
		else { $scope.filter.minradius = name2;}
		//////console.log($scope.filter.maxradius);
		//////console.log($scope.filter.minradius);
	};

	//assign values
	$scope.beertype = function(name1){
		if (name1 == 'lager') {$scope.lager = name1;  $scope.ale = null; $scope.lambic = null; $scope.beer= null;}
		else if (name1 == 'ale') {$scope.lager = null;  $scope.ale = name1; $scope.lambic = null; $scope.beer = null;}
		else if (name1 == 'lambic') {$scope.lager = null;  $scope.ale = null; $scope.lambic = name1; $scope.beer = null;}
		else { $scope.lager = null;  $scope.ale = null; $scope.lambic = null;	$scope.beer = null;}
		////console.log($scope.lager);
		////console.log($scope.ale);
		////console.log($scope.lambic);
	};

	//assign values
	$scope.beerchar = function(name1) {

		if (name1 != 'none') { $scope.beer = name1;}
		else { $scope.beer = null;}
		////console.log($scope.beer);
	
	};

	//assign values
	$scope.winecolour = function(name1) {

		if (name1 != 'none') { $scope.colour = name1;}
		else { $scope.colour = null;}
		////console.log($scope.colour);
	
	};

	//assign values
	$scope.winesugar = function(name1) {

		if (name1 != 'none') { $scope.sugar = name1;}
		else { $scope.sugar = null;}
		////console.log($scope.sugar);
	
	};

	//assign values
	$scope.winecarbon = function(name1) {

		if (name1 != 'none') { $scope.carbon = name1;}
		else { $scope.carbon = null;}
		////console.log($scope.carbon);
	
	};

		//assign values
	$scope.whingredients = function(name1) {

		if (name1 != 'none') { $scope.ingredients = name1;}
		else { $scope.ingredients = null;}
		////console.log($scope.ingredients);
	
	};

	//assign values
	$scope.whorigin = function(name1) {

		if (name1 =='none') { $scope.origin = null;}
		else if( name1 == 'american') {	$scope.origin = name1;}
		else { $scope.origin = name1; $scope.american = null;}
		////console.log($scope.origin);
	
	};
	
	//assign values
	$scope.whamerican = function(name1) {

		if (name1 != 'none') { $scope.american = name1;}
		else { $scope.american = null;}
		////console.log($scope.american);
	
	};

	var onPosition = function(position)
	{
		 latitude2 = position.coords.latitude;
		 longitude2 = position.coords.longitude;

						
		var geocodingAPI = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+ latitude2 + ',' + longitude2 + "&key=AIzaSyCxthBcp0Dzs9cNbJ6nKe-S1EOhxW77PNw&sensor=true";
		//diavazei to json arxeio k vriskei to address,long,lat
		$.getJSON(geocodingAPI, function (json) {
			 var status = json.status;

			 if(status != 'OK') { alert('Η διεύθυνση που εισαγάγατε δεν υπάρχει. Δοκιμάστε ξανά.');}
			 else{
			 // Set the variables from the results array
			  address2 = json.results[0].formatted_address;
			 ////console.log('Address : ', address2);
		$scope.filter.location = address2;

			 
			  latitude2 = json.results[0].geometry.location.lat;
			  $scope.geo.lat = latitude2
			 ////console.log('Latitude : ', latitude2);
			 
			  longitude2 = json.results[0].geometry.location.lng;
			  $scope.geo.long = longitude2
			 ////console.log('Longitude : ', longitude2);


		
		
		  
		  document.getElementsByName('address2')[0].value=address2;
		
		  help2 = address2;
		 
		  }
		})



	}
	$scope.onPositionUpdate = function(){
	if(navigator.geolocation)
		navigator.geolocation.getCurrentPosition(onPosition);
	else
		alert("navigator.geolocation is not available");
	}
	
	$scope.add = function(name1)
              {
                  ////console.log("hi")
                 if (help2 == null || name1 != null)
                {
                     $scope.filter.location = name1 ;
                }
                else{
                    $scope.filter.location = help2 ;
                    help2 = null;
                 }
                //////console.log($scope.filter.location)
                var geocodingAPI = "https://maps.googleapis.com/maps/api/geocode/json?address="+ String($scope.filter.location)  + "&key=AIzaSyCxthBcp0Dzs9cNbJ6nKe-S1EOhxW77PNw&sensor=true";

               $.getJSON(geocodingAPI, function (json) {
                    var status = json.status;

                    
                    
                    // Set the variables from the results array
                    address2 = json.results[0].formatted_address;
                    //console.log('Address : ', address2);
					 latitude2 = json.results[0].geometry.location.lat;
					 $scope.geo.lat = latitude2;
                    ////console.log('Latitude : ', latitude2);
                    
					longitude2 = json.results[0].geometry.location.lng;
					$scope.geo.long = longitude;
                    ////console.log('longitude2 : ', longitude2);

                    
              
                    
                });

         }
	$scope.setAddress = function(){
		$scope.filter.location = address2;
	}

	//get final results-tags
	$scope.submit = function(){
		
		var beerTags = "";
		var wineTags = "";
		var whiskeyTags = "";
		var products = "";

		var url="search?";
		var tags = "";

		if($scope.filter.beer == "beer"){
			beerTags += "Μπύρα&"
			if($scope.lager != "" && $scope.lager != "none" && $scope.lager != null)
				{	beerTags += $scope.lager + "&";
					//url += "beerTags=" + $scope.lager + "&";
				}
			else if($scope.ale != "" && $scope.ale != "none" && $scope.ale != null)
				{	beerTags += $scope.ale + "&";
					//url += "beerTags=" + $scope.ale + "&";
				}
			else if($scope.lambic != "" && $scope.lambic != "none" && $scope.lambic != null)
				{	beerTags += $scope.lambic + "&";
				//	url += "beerTags=" + $scope.lambic + "&";
				}

			if($scope.beer != "" && $scope.beer != "none" && $scope.beer != null)
				{	beerTags += $scope.beer + "&";
				//	url += "beer.char=" + $scope.beer + "&";
			}
		}
		if($scope.filter.whiskey == "whiskey"){
			whiskeyTags = "Ουίσκι&";
			if($scope.ingredients != "" && $scope.ingredients != "none" && $scope.ingredients != null)
				{	whiskeyTags += $scope.ingredients + "&";
					//url += "whiskey.ingred=" + $scope.ingredients + "&";
				}
			if($scope.origin != "" && $scope.origin != "none" && $scope.origin != null){
				whiskeyTags += $scope.origin + "&";
				//url += "whiskey.origin=" + $scope.origin + "&";
				if($scope.origin == "american" && $scope.american != "" && $scope.american != "none" && $scope.american != null)
					whiskeyTags += $scope.american + "&";
					//url += "whiskey.american=" + $scope.american + "&";
				}
			}
		if($scope.filter.wine == "wine"){
			wineTags = "Οίνος/Κρασί&"

			if($scope.colour != "" && $scope.colour != "none" && $scope.colour != null)
				{	wineTags += $scope.colour + "&" ;
					//url += "wine.colour=" + $scope.colour + "&";
				}
			if($scope.sugar != "" && $scope.sugar != "none" && $scope.sugar != null)
				{	wineTags += $scope.sugar + "&" ;
					//url += "wine.sugar=" + $scope.sugar + "&";
				}
			if($scope.carbon != "" && $scope.carbon != "none" && $scope.carbon != null)
				{	wineTags += $scope.carbon + "&" ;
					//url += "wine.carbon=" + $scope.carbon + "&";
				}
				////console.log(wineTags)
			}
		
		if($scope.filter.gin == "gin"){
			tags += "Τζίν&"	
		}
		
		if($scope.filter.vodka == "vodka"){
			tags += "Βότκα&"	
		}
		
		if($scope.filter.other == "other"){
			tags += $scope.otherChoice + "&"	
		}

		tags += wineTags + beerTags + whiskeyTags;
		
		if($scope.filter.maxradius)
			url += "geoDist=" + $scope.filter.maxradius + "&";
		if($scope.geo.long && $scope.geo.lat){
			if ($scope.geo.long < 23.453312 || $scope.geo.long > 23.980449 || $scope.geo.lat < 37.752946 || $scope.geo.lat > 38.143808){
				url += "geoLng=23.727539&";
				url += "geoLat=37.983810&";
			}
			else {
				url += "geoLng=" + $scope.geo.long + "&";
				url += "geoLat=" + $scope.geo.lat + "&";
			}
		}
		else{
			url += "geoLng=23.727539&";
			url += "geoLat=37.983810&";
		}
		if ($scope.filter.dateFrom){
			var df = $filter('date')($scope.filter.dateFrom,"yyyy-MM-dd")
			url+="dateFrom=" + df + "&"
		}
		else if($scope.params.dateFrom)
			url+="dateFrom=" + $scope.params.dateFrom + "&"
		else
			url+="dateFrom=" + today + "&"
		if ($scope.filter.dateTo){
			var df = $filter('date')($scope.filter.dateTo,"yyyy-MM-dd")
			url+="dateTo=" + df + "&"
		}
		else if($scope.params.dateTo)
			url+="dateTo=" + $scope.params.dateTo + "&"
		else
			url+="dateTo=" + today + "&"
		//url+= tags;
		tags = tags.split("&")
		for( let i in tags){
			if (tags[i] != "")
			url += "tags=" + tags[i] + "&";
		}
		//console.log(url)
		if($scope.params.page && $scope.params.page != "1")
			url += "page=" + $scope.params.page + "&";
		if($scope.filter.minprice)
			url+="min=" + $scope.filter.minprice + "&";
		if($scope.filter.maxprice)
			url+="max=" + $scope.filter.maxprice + "&";
		if($scope.filter.location)
			url += "address=" + $scope.filter.location + "&";
		$location.url(url);
		/*
		var config = {
			responseType: "json",
		}

		//console.log(url);
		$http.get(url, config).then(
			function success(response){
				
				$scope.params.start = response.data.start;
				$scope.params.count = response.data.count;
				$scope.params.total = response.data.total;
				$scope.params.page = $scope.params.start / $scope.params.count + 1;
				$scope.lastPage = Math.floor(response.data.total / $scope.params.count) + 1;
				$scope.prices= response.data.prices;
				//////console.log($scope.prices)
				var temp = new Set();
				for (price of $scope.prices){
					temp.add(price.shopId)
				}
				ids = [...temp]
				////console.log(ids)
			},
			function fail(response){
				////console.log(response)
				////console.log("Failed to retrieve price resources");
			}
		);
		*/
	}
	$scope.tags = function(name, val){
		if (Array.isArray($scope.params.tags))
			for( let i in $scope.params.tags){
				if(name == i){
					console.log(val)
					$scope.filter[val] = val;
					return true;
				}
			}
		else
			if (name == $scope.params.tags){
				console.log(val)
				$scope.filter[val] = val;
				return true;
			}
		return false;
	}
};
