var errorCtrl = function($scope, $location, $routeParams){
	$scope.status = null
	$scope.text = null

	if($location.path().indexOf("/error") == 0){
		if($routeParams["status"]){
			$scope.status = $routeParams["status"];
			document.title = "HoS - Error " + $scope.status
		}
		if($routeParams["text"]){
			$scope.text = $routeParams["text"];
		}
	}
};
