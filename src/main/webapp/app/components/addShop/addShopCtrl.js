var addShopCtrl = function($scope, $http, $location, $httpParamSerializer, $rootScope){
	//var url = "app/components/addShop/Shops.json";
	document.title = "HoS - Προσθήκη Καταστήματος"
	$scope.oldShop = {
		found: false,
		id: null,
		name: null,
		address: null,
	};
	$scope.newShop = {
		name: null,
		address: null,
		tags: null,
		lat: null,
		lng: null
	}
	$scope.newShop = {
		id: null,
		photo: null,
		name: null,
		adress: null,
		lng: null,
		lat: null,
		tags: null
	}
	$scope.disable = {
		scan: false,
		message: true
	}
		
	var end = null;
	
	$scope.reset = function(){
		$scope.newShop = {
			name: null,
			address: null,
			tags: null,
			lat: null,
			lng: null
		}
	}

	$scope.addressQuery = function(){
	}

	$scope.showMap = function(){
		$("#mapModal").modal("show");
	}

	$scope.ok = function () {
        console.log(address);
        console.log(latitude);
        console.log(longitude);
		$scope.newShop.address = address
		$scope.newShop.lng = longitude
		$scope.newShop.lat = latitude
		$("#mapModal").modal("hide");
	};

  	$scope.cancel = function () {
		$("#mapModal").modal("hide");
  	};

	$scope.modal = function(name, type){
		if(type == "show")
			$("#"+ name).modal("show");
		else if(type == "hide")
			$("#"+ name).modal("hide");
	}
	$scope.goToShop = function(name, id){
		$("#" + name).modal("hide");
		$("#" + name).on("hidden.bs.modal", function () {
			$scope.$apply(function(){
				$location.url("/shop/" + id);
			});
		});
	}

	$scope.goToMain = function(){
		$("#barcodeQueryModal").modal("hide");
		$("#barcodeQueryModal").on("hidden.bs.modal", function () {
			$scope.$apply(function(){
				$location.url("/mainPage");
			});
		});
	}

	$scope.coordinates = {
		longitude: null,
		latitude: null
	}
	$scope.error = {
		show : false,
		GPS: null
	};
	$scope.closeShops = {
		names : null,
		ids : null
	}

	$scope.submit = function(){
		
		var finalTags = $scope.newShop.tags;
		finalTags = finalTags.split(" ");
		var url = "api/shops";
		var data = {
			name: $scope.newShop.name,
			tags: finalTags,
			address: $scope.newShop.address,
			lng: $scope.newShop.lng,
			lat: $scope.newShop.lat
		}
		config = {
			//response: "json"
			headers : {
				"X-OBSERVATORY-AUTH" : $rootScope.token,
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}
		data = $httpParamSerializer(data);
		$http.post(url, data, config).then(
			function success(response){
				$scope.newShop.id = response.data.id;
				$scope.modal("addSuccessModal", "show");
			},
			function error(response){
				//console.log(response);
				var error = "status=" + response.status
				if(response.statusText)
				 	error += "&text=" + response.statusText;
				$location.url("error?" + error)
			}
		)
	};
};

