var myStyles=[
  {
    featureType: "poi",
    elementType: "labels.text",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "poi.business",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "road",
    elementType: "labels.icon",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "transit",
    stylers: [
      {
        visibility: "off"
      }
    ]
  }
];


var help1 = null;
var address;
var longitude;
var latitude;

          //Angular App Module and Controller
          var mapApp = angular.module('mapApp', []);
          mapApp.controller('MapController', function ($scope) {
      
              
           var onPosition = function(position)
            {
                 latitude = position.coords.latitude;
                 longitude = position.coords.longitude;

                                
                var geocodingAPI = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+ latitude + ',' + longitude + "&key=AIzaSyCxthBcp0Dzs9cNbJ6nKe-S1EOhxW77PNw&sensor=true";

                $.getJSON(geocodingAPI, function (json) {
                     var status = json.status;
 
                     if(status != 'OK') { alert('adress doesnt exists');}
                     else{
                     // Set the variables from the results array
                      address = json.results[0].formatted_address;
                     //console.log('Address : ', address);
                     
                      latitude = json.results[0].geometry.location.lat;
                     //console.log('Latitude : ', latitude);
                     
                      longitude = json.results[0].geometry.location.lng;
                     //console.log('Longitude : ', longitude);


                var city= [
                  {
                      place :  address,
                      lat : latitude,
                      long : longitude
                  }];
                  var mapOptions = {
                      zoom: 18,
                      center: new google.maps.LatLng(latitude,longitude),
                      mapTypeId: google.maps.MapTypeId.ROADMAP,
                      styles: myStyles
                  }
                
                  $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

                  createMarker(city[0]);
                  //$('#address').value(address);
                  document.getElementsByName('address')[0].value=address;
                  help1 = address;
                    $(function() {
                      window.parent.notifyTextChanged(address, latitude, longitude);
                    });                  
                  kostakis();
                  }
                })



            }
            $scope.onPositionUpdate = function(){
            if(navigator.geolocation)
                navigator.geolocation.getCurrentPosition(onPosition);
            else
                alert("navigator.geolocation is not available");
            }
            
             
            
              var mapOptions = {
                  zoom: 10,
                  center: new google.maps.LatLng(37.983810,23.727539),
                  mapTypeId: google.maps.MapTypeId.ROADMAP,
                  styles: myStyles
              }
            
              $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

              var kostakis = function (){

              google.maps.event.addListener($scope.map, 'click',function(event){

             //   document.getElementsByName('submitButton').;
                
                var geocodingAPI = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+ event.latLng.lat() + ',' + event.latLng.lng() + "&key=AIzaSyCxthBcp0Dzs9cNbJ6nKe-S1EOhxW77PNw&sensor=true";

                $.getJSON(geocodingAPI, function (json) {
                     var status = json.status;
 
                     if(status != 'OK') { alert('adress doesnt exists');}
                     else{
                     // Set the variables from the results array
                     address = json.results[0].formatted_address;
                     //console.log('Address : ', address);
                     
                     latitude = json.results[0].geometry.location.lat;
                     //console.log('Latitude : ', latitude);
                     
                     longitude = json.results[0].geometry.location.lng;
                     //console.log('Longitude : ', longitude);


                var city= [
                  {
                      place :  address,
                      lat : event.latLng.lat(),
                      long : event.latLng.lng()
                  }];
                  var mapOptions = {
                      zoom: 18,
                      center: new google.maps.LatLng(event.latLng.lat(),event.latLng.lng()),
                      mapTypeId: google.maps.MapTypeId.ROADMAP,
                      styles: myStyles
                  }
                
                  $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

                  createMarker(city[0]);
                  //$('#address').value(address);
                  document.getElementsByName('address')[0].value=address;
                 help1 = address;
                    $(function() {
                      window.parent.notifyTextChanged(address, latitude, longitude);
                    });            
                  kostakis();
                  }
                })
             } )
        };  
              $scope.markers = [];
              
              var infoWindow = new google.maps.InfoWindow();
              
              var createMarker = function (info){
                  
                  var marker = new google.maps.Marker({
                      map: $scope.map,
                      position: new google.maps.LatLng(info.lat, info.long),
                      title: info.place
                  });
                  
                  google.maps.event.addListener(marker, 'click', function(){
                      infoWindow.setContent('<h2>' + marker.title);
                      infoWindow.open($scope.map, marker);
                  });
                  
                  $scope.markers.push(marker);
                  
              }
              kostakis();
              
              
            //  for (i = 0; i < cities.length; i++){
              //    createMarker(cities[i]);
              //}

            $scope.add = function(name1)
              {
                  //console.log("hi")
                 if (help1 == null || name1 != null)
                {
                     $scope.map.address = name1 ;
                }
                else{
                    $scope.map.address = help1 ;
                    help1 = null;
                 }
                //console.log($scope.map.address)
                var geocodingAPI = "https://maps.googleapis.com/maps/api/geocode/json?address="+ String($scope.map.address)  + "&key=AIzaSyCxthBcp0Dzs9cNbJ6nKe-S1EOhxW77PNw&sensor=true";

               $.getJSON(geocodingAPI, function (json) {
                    var status = json.status;

                    if(status != 'OK') { alert('adress doesnt exists');}
                    else{
                    // Set the variables from the results array
                     address = json.results[0].formatted_address;
                    //console.log('Address : ', address);
                    
                     latitude = json.results[0].geometry.location.lat;
                    //console.log('Latitude : ', latitude);
                    
                    longitude = json.results[0].geometry.location.lng;
                    //console.log('Longitude : ', longitude);

                    var city= [
                        {
                            place :  address,
                            lat : latitude,
                            long : longitude
                        }];
               //   for (i = 0; i < cities.length; i++){
                 // createMarker(cities[i]);
              //}    
                   

                    var mapOptions = {
                        zoom: 18,
                        center: new google.maps.LatLng(latitude,longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        styles: myStyles
                    }
                  
                    $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

                    createMarker(city[0]);
                    $(function() {
                      window.parent.notifyTextChanged(address, latitude, longitude);
                    });
                    kostakis();
                //console.log(json.results[0]);
                    }
                });

                
                

              //  console.log($scope.lat); 

              }

              function zipPhoneCallback(address) {
                ((console&&console.log)||alert)("address = " + address);
                }




          });
