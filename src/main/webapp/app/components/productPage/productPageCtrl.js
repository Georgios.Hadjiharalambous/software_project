var productPageCtrl = function($scope, $http, $location, $routeParams, $filter){
	$scope.params = {
		id: null,
		page: 1,
		start: 0,
		count: 20,
		geoDist: null,
		geoLng: null,
		geoLat: null,
		dateFrom:  null,
		dateTo: null,
		shops: null,
		tags: null,
		sort: "price|ASC"
	};
	$scope.new = {
		dateFrom : null,
		dateTo : null
	}
	$scope.product = {
		id: null,
		name: null,
		description: null,
		category: null,
		tags: [],
		withdrawn: null,
		extraData: {
			quantity_ml: null,
			alcohol: null,
			barcode_id: null
		}
	};
	$scope.shops = [
		{
			id: null,
			name: null,
			address: null,
			lng: null,
			lat: null,
			tags: [],
			withdrawn: false

		}
	]; //initialize so it is not empty thus hiding the "no shops message before loading"

	$scope.lastPage = 1;

	$scope.newPrice = function(){
		$scope.setLocation("addPrice?product=" + $scope.product.id)
	}

	$scope.newUrl = function(){
		var today = new Date();
		today = $filter('date')(today,"yyyy-MM-dd");
		var url = "/product/" + $scope.params.id + "?";
		if($scope.params.page && $scope.params.page != "1")
			url += "page=" + $scope.params.page + "&";
		if($scope.params.count && $scope.params.count != "20")
			url += "count=" + $scope.params.count + "&";
		if($scope.params.geoDist)
			url += "geoDist=" + $scope.params.geoDist + "&";
		if($scope.params.geoLng)
			url += "geoLng=" + $scope.params.geoLng + "&";
		if($scope.params.geoLat)
			url += "geoLat=" + $scope.params.geoLat + "&";
		if($scope.new.dateFrom){
			var temp = $filter('date')($scope.new.dateFrom,"yyyy-MM-dd");
			url += "dateFrom=" + temp + "&";
		}
		else if ($scope.params.dateFrom)
			url += "dateFrom=" + $scope.params.dateFrom + "&";
		else
			url += "dateFrom=" + today + "&";
		if($scope.new.dateTo){
			var temp = $filter('date')($scope.new.dateTo,"yyyy-MM-dd");
			url += "dateTo=" + temp + "&";
		}
		else if($scope.params.dateTo)
			url += "dateTo=" + $scope.params.dateTo + "&";
		else
			url += "dateTo=" + today + "&";
		if($scope.params.shops){
			for(var i = 0; i < $scope.params.shops.length; i++){
				url += "shops=" + i + "&";
			}
		}
		if($scope.params.sort){
			if (Array.isArray($scope.params.sort))
				for( let i in $scope.params.sort){
					url += "sort=" + $scope.params.sort[i] + "&";
				}
			else
				url += "sort=" + $scope.params.sort + "&";
		}
		return url;
	}
	
	$scope.changeSort = function(type, dir){
		$scope.params.sort = type + "|" + dir;
		$location.url($scope.newUrl());	
	}
	
	$scope.changePage = function(page){
		//page I want to go starting at 1
		if (page < $scope.lastPage + 1 && page > 0){
			$scope.params.page = page;
			$location.url($scope.newUrl());	
		}
	}

	$scope.getPrices = function(){
		var url="api/prices?";
		url += "products=" + $scope.params.id + "&";
		if($scope.params.page && $scope.params.page != "1")
			url += "start=" + ($scope.params.page - 1) * $scope.params.count + "&";
		if($scope.params.count)
			url += "count=" + $scope.params.count + "&";
		if($scope.params.geoDist)
			url += "geoDist=" + $scope.params.geoDist + "&";
		if($scope.params.geoLng)
			url += "geoLng=" + $scope.params.geoLng + "&";
		if($scope.params.geoLat)
			url += "geoLat=" + $scope.params.geoLat + "&";
		if($scope.params.dateFrom)
			url += "dateFrom=" + $scope.params.dateFrom + "&";
		if($scope.params.dateTo)
			url += "dateTo=" + $scope.params.dateTo + "&";
		if($scope.params.shops){
			for(var i = 0; i < $scope.params.shops.length; i++){
				url += "shops=" + i + "&";
			}
		}
		if($scope.params.tags){
			for(var i = 0; i < $scope.params.tags.length; i++){
				url += "tags=" + i + "&";
			}
		}
		url += "sort=" + $scope.params.sort;
		//console.log($scope.params)
		//console.log(url)
		var config = {
			responseType: "json",
		};
		$http.get(url, config).then(
			function success(response){
				$scope.shops = response.data.prices;
				$scope.lastPage = Math.floor(response.data.total / $scope.params.count) + 1;
				$scope.params.count = response.data.count;
				var temp = new Set();
				for (price of $scope.shops){
					temp.add(price.shopId)
				}
				ids = [...temp]
				//console.log(ids)
			},
			function fail(response){
				//console.log("Failed to retrieve prices resource");
				//console.log(response);
				var error = "status=" + response.status
				if(response.statusText)
					error += "&text=" + response.statusText;
				$location.url("error?" + error)
			}
		);
	}

	if($location.path().indexOf("/product") == 0){

		if($routeParams["id"])
			$scope.params.id = $routeParams["id"];
		if($routeParams["page"]){
			var p = $routeParams["page"];
			$scope.params.page = parseInt(p);
		}
		if($routeParams["start"])
			$scope.params.start = $routeParams["start"];
		if($routeParams["count"])
			$scope.params.count = $routeParams["count"];
		if($routeParams["geoDist"])
			$scope.params.geoDist = $routeParams["geoDist"];
		if($routeParams["geoLng"])
			$scope.params.geoLng = $routeParams["geoLng"];
		if($routeParams["geoLat"])
			$scope.params.geoLat = $routeParams["geoLat"];
		if($routeParams["dateFrom"])
			$scope.params.dateFrom = $routeParams["dateFrom"];
		if($routeParams["dateTo"])
			$scope.params.dateTo = $routeParams["dateTo"];
		if($routeParams["shops"])
			$scope.params.shops = $routeParams["shops"];
		if($routeParams["tags"])
			$scope.params.tags = $routeParams["tags"];
		if($routeParams["sort"])
			$scope.params.sort = $routeParams["sort"];
		
		$scope.image = "assets/img/generic.jpg"
		//var url = "main/products/" + $scope.search.id
		//var url = "app/components/productPage/productExample.json"
		var url="api/products/" + $scope.params.id;
		//console.log(url)
		var config = {
			responseType: "json",
		}
		$http.get(url, config).then(
			function success(response){
                $scope.product = response.data;
				document.title = "HoS - " + $scope.product.name;
				if ($scope.product.category == "Βότκα")
					$scope.image = "assets/img/vodka.jpg";
				else if ($scope.product.category == "Μπύρα")
					$scope.image = "assets/img/beer.jpg";
				else if ($scope.product.category == "Ρούμι")
					$scope.image = "assets/img/rum.jpg";
				else if ($scope.product.category == "Ρακή")
					$scope.image = "assets/img/raki.jpg";
				else if ($scope.product.category == "Τσίπουρο")
					$scope.image = "assets/img/tsipouro.jpg";
				else if ($scope.product.category == "Τεκίλα")
					$scope.image = "assets/img/tequila.jpg";
				else if ($scope.product.category == "Οίνος/Κρασί")
					$scope.image = "assets/img/wine.jpg";
				else if ($scope.product.category == "Ουίσκι")
					$scope.image = "assets/img/whiskey.png";
				else if ($scope.product.category == "Τζίν")
					$scope.image = "assets/img/gin.jpg";
				else
					$scope.image = "assets/img/generic.jpg";
				//console.log($scope.image)
			},
			function fail(response){
				//console.log("Failed to retrieve product resource");
				//console.log(response);
				var error = "status=" + response.status
				if(response.statusText)
					error += "&text=" + response.statusText;
				$location.url("error?" + error)
			}
		);
		$scope.getPrices();
	}

	$scope.showMap = function(){
		$("#mapModal").modal("show");
	}

	$scope.cancel = function () {
		$("#mapModal").modal("hide");
  	};

	$scope.ok = function () {
        //console.log(address);
        //console.log(latitude);
        //console.log(longitude);
		$scope.newShop.address = address
		$scope.newShop.lng = longitude
		$scope.newShop.lat = latitude
		$("#mapModal").modal("hide");
	};
};

