var addProductCtrl = function($scope, $http, $location, $routeParams, $rootScope, $httpParamSerializer){
	document.title = "HoS - Προσθήκη Προϊόντος"
	//var url = "app/components/addProduct/products.json";
	$scope.beer = "";
	$scope.lager = "";
	$scope.ale = "";
	$scope.lambic = "";
	$scope.origin = "";
	$scope.ingredients = "";
	$scope.american = "";
	$scope.colour = "";
	$scope.sugar = "";
	$scope.carbon = "";

	$scope.oldProduct = {
		found: false,
		id: null,
		name: null,
		category: null,
		quantity_ml: null,
		alcohol: null
	};
	$scope.barcodeError = false;
	$scope.newProduct = {
		id: null,
		barcode: null,
		photo: null,
		name: null,
		quantity_ml: null,
		alcohol: null,
		description: null,
		type: null,
		tags: null
	}

	
	$scope.disable = {
		scan: false,
		message: true
	}
	ScanditSDK.configure("AdN8nSDjPA5+RD4fsw0/i5QIhIDaNRp3IVcfL4QLE8uocZTkpxzsCvMB3s/uX6bwgUty4qhJk88MVhMcfFHp0VouuWt1WRL07HjApONTKaYRQQtVYl2+1pkCWbpBRhHYLjMzJDp0iJ58Owpxu7ysGoOZK0GxIZMroQkE/Nslq94c1w9HLyM/wsjVEmhv/9nB41NXjemGF8m5wJakKnJh+ykNieiV9kTErU5nA0L6MtyMWTqubfO5bcMIds0AnV8GqPETBHjAC9bb+rekHDGhkrQJ1ZKBoQjESeUV9H1JEzC8Y1YamJZZfwUCpTVJFic4Bphn9f0hTWwRdsFwGEhkuRj3KVuIbn0CnfypfM/7QpZa6CbsELHSoxpuz8NkwnMax1SjNZ8P3GDLGgfWblilG3Kma3HKd0y7UbRU70Xf6QmHP2f/9rfRJNW9qii0MzNHDBjXYwH/6xhQ4+QhHaRb2Y2No+Q4q/wvnhEuDd4hDufUh1994g5J98zmdfBiQwV9aVCo981xJ9qyEix4HW7mH2KERzPduzb5FCbs5fsAuP9JG6LXhBhDSM5r2yDVu3b+sGLTYxMPv3FO2IwJx021HDcmplL4szFQDh0sVoayxWaz2/J4sBXrVh7IoUjVje9t9v0w3k106McYjyZ401pUcqKCmATl/FckOaxn7aDikiMSA5wTa6WXUzICDjmRwgJoHu78bbKP5kJV7gXXiP2zd0u+WB1bsoDEkfPD772OQlWbxhvjk//3To9UXgYnAjaPdUYxlasTOn5RiEa4VDzzFZ/52q9bXDFmpBHbNbTmoC0EuA==", {
		engineLocation: "https://cdn.jsdelivr.net/npm/scandit-sdk/build",
	})
	
	var end = null;
	$scope.scan = function(){
		$scope.disable.scan = true;
		picker = ScanditSDK.BarcodePicker;
		picker.create(document.getElementById("scandit-barcode-picker"), {
			playSoundOnScan: false,
			vibrateOnScan: true
		}).then(function(barcodePicker) {
			// barcodePicker is ready here to be used
			var scanSettings = new ScanditSDK.ScanSettings({
				enabledSymbologies: ["ean8", "ean13"],
				codeDuplicateFilter: 1000
			});
			end = barcodePicker;
			barcodePicker.applyScanSettings(scanSettings);
			barcodePicker.onScan(function(scanResult) {
				scanResult.barcodes.reduce(function(string, barcode) {
					$scope.$apply(function(){
						$scope.newProduct.barcode = Number(barcode.data);
						$scope.disable.bar = true;
						$scope.barcodeQuery();
						
					});
				}, "");
				this.destroy();
				$scope.$apply( 
					function(){
						$scope.disable.scan = false;
					}
				);
			});
		}
		);
	}
	$scope.reset = function(){
		$scope.newProduct = {
			barcode: null,
			photp: null,
			name: null,
			quantity_ml: null,
			alcohol: null,
			description: null,
			type: null,
			labels: null
		}
		$scope.disable = {
			scan: false,
			message: true
		}
	}

	$scope.barcodeQuery = function(){
		if($scope.newProduct.barcode != ""){
			$scope.disable.message = false;
			var url = "ProductController";
			var config = {
				response: "json",
				params: {
					action: "barcodes",
					barcodes: [$scope.newProduct.barcode]
				}
			}
			$http.get(url,config).then(
				function success(response){
					if(response.data.total == 1){
						$scope.oldProduct = {
							found: true,
							id: response.data.products[0].id,
							name: response.data.products[0].name,
							category: response.data.products[0].category,
							quantity_ml: response.data.products[0].extraData.quantity_ml,
							alcohol: response.data.products[0].extraData.alcohol
						}
						$scope.reset(); 
						//I won't be needing the form data again.
						//I visit product page or add new Product
					}	
					$scope.modal("barcodeQueryModal", "show");
				},
				function error(response){
					$scope.barcodeError = true;
					$scope.modal("barcodeQueryModal", "show");
				}
			)
		}
	}
	if($location.path().indexOf("/addProduct") == 0){
		if($routeParams["barcode"]){
			$scope.newProduct.barcode = Number($routeParams["barcode"]);
			$scope.barcodeQuery()
		}
	}

	$scope.modal = function(name, type){
		if(type == "show")
			$("#"+ name).modal("show");
		else if(type == "hide")
			$("#"+ name).modal("hide");
	}
	$scope.goToProduct = function(name, id){
		$("#" + name).modal("hide");
		$("#" + name).on("hidden.bs.modal", function () {
			$scope.$apply(function(){
				$location.url("/product/" + id);
			});
		});
	}

	$scope.goToMain = function(){
		$("#barcodeQueryModal").modal("hide");
		$("#barcodeQueryModal").on("hidden.bs.modal", function () {
			$scope.$apply(function(){
				$location.url("/mainPage");
			});
		});
	}

	$scope.cancel = function(){
		if(end !== null) end.destroy();
		else $("#scandit-barcode-picker").html("");
		$scope.disable.scan = false;
	}
	$scope.coordinates = {
		longitude: null,
		latitude: null
	}
	$scope.error = {
		show : false,
		GPS: null
	};
	$scope.closeShops = {
		names : null,
		ids : null
	}

	//var url ="ControllerProducts"; //ControllerProducts returns all products with name and types
	//I decide that it is not very useful having suggestions of all names but types are useful and I can create a list
	var url = "productTypes.json";
	$http.get(url, {
		responseType: "json"
	}).then(
		function successCallback(response){
			$scope.productTypes = response.data
		},
		function errorCallBack(response){
			var error = "status=" + response.status
			if(response.statusText)
				error += "&text=" + response.statusText;
			$location.url("error?" + error)
		}
	);
	
	

	$scope.submit = function(){
		var autoTags = $scope.newProduct.type+ " "; 
		if($scope.newProduct.type == "Μπύρα"){
			if($scope.beer != "" && $scope.beer != "none"){
				autoTags += $scope.beer + " ";
				if($scope.beer == "lager" && $scope.lager != "" && $scope.lager != "none")
					autoTags += $scope.lager + " ";
				else if($scope.beer == "ale" && $scope.ale != "" && $scope.ale != "none")
					autoTags += $scope.ale + " ";
				else if($scope.beer == "lambic" && $scope.lambic != "" && $scope.lambic != "none")
					autoTags += $scope.lambic + " ";
			}
		}
		else if($scope.newProduct.type == "Ουίσκι"){
			if($scope.ingredients != "" && $scope.ingredients != "none")
				autoTags += $scope.ingredients + " ";
			if($scope.origin != "" && $scope.origin != "none"){
				autoTags += $scope.origin + " ";
				if($scope.origin == "american" && $scope.american != "" && $scope.american != "none")
					autoTags += $scope.american + " ";
			}
		}
		else if($scope.newProduct.type == "Οίνος/Κρασί"){
			if($scope.colour != "" && $scope.colour != "none")
				autoTags += $scope.colour + " ";
			if($scope.sugar != "" && $scope.sugar != "none")
				autoTags += $scope.sugar + " ";
			if($scope.carbon != "" && $scope.carbon != "none")
				autoTags += $scope.carbon + " ";
		}
		var finalTags = autoTags + $scope.newProduct.tags;
		finalTags = finalTags.split(" ");
		var url = "api/products";
		var data = {
			name: $scope.newProduct.name,
			category: $scope.newProduct.type,
			description: $scope.newProduct.description,
			tags: finalTags,
			alcohol: $scope.newProduct.alcohol,
			quantity_ml: $scope.newProduct.quantity_ml,
			barcode_id: $scope.newProduct.barcode
		}
		var config = {
			headers : {
				"X-OBSERVATORY-AUTH" : $rootScope.token,
				'Content-Type': 'application/x-www-form-urlencoded'
			}
			//responseType: "json"
		}
		data = $httpParamSerializer(data);
		$http.post(url, data, config).then(
			function success(response){
				$scope.newProduct.id = response.data.id;
				$scope.modal("addSuccessModal", "show");
			},
			function error(response){
				//console.log(response.data);
				var error = "status=" + response.status
				if(response.statusText)
				 	error += "&text=" + response.statusText;
				$location.url("error?" + error)
			}
		)
	};
};

