var address;
var latitude;
var longitude;

function notifyTextChanged(iaddress,ilatitude,ilongitude) {
    address = iaddress;
    latitude = ilatitude;
    longitude = ilongitude;
    
}

var ids = [];

angular.module("cavaApp", ["ngRoute", ]);
angular.module("cavaApp")
.controller("cavaAppCtrl", function($location,$scope, $rootScope){
	$scope.addProductPage = function(){
		$location.url("/addProduct")
	};
	$scope.setLocation = function(url){
		//$location.url("/" + url);
		location.assign(location.origin + "/observatory/#!/" + url)
	};
	$rootScope.setCookie = function(cname, cvalue, exdays){
		if (exdays != 0){
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+ d.toUTCString();
			document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/observatory";
		}
		else {
			document.cookie = cname + "=" + cvalue + ";path=/observatory";
		}
	}
	$rootScope.getCookie = function(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
		  		c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
		  		return c.substring(name.length, c.length);
			}
		}
		return "";
	}
	$rootScope.deleteCookie = function(cname){
		document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/observatory;";
	}

	$rootScope.user = $scope.getCookie("username");
	$rootScope.token = $scope.getCookie("token")
	if($rootScope.user != ""){
		$rootScope.logged = true;
	}
	else $rootScope.logged = false;
	console.log($rootScope.logged);
})
.controller("addProductCtrl", addProductCtrl)
.controller("addShopCtrl", addShopCtrl)
.controller("mainPageCtrl", mainPageCtrl)
.controller("searchProductCtrl", searchProductCtrl)
.controller("productPageCtrl", productPageCtrl)
.controller("shopPageCtrl", shopPageCtrl)
.controller("addPriceCtrl", addPriceCtrl)
.controller("errorCtrl", errorCtrl)
.controller("headerCtrl", headerCtrl);
