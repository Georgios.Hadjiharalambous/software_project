var headerCtrl = function($location, $scope, $rootScope, $http, $httpParamSerializer){
	//This is a hack to show active page
	$scope.setLocation = function(url){
		location.assign(location.origin + "/observatory/#!/" + url)
		$("#headerNavbar #navList li").removeClass("active");
		$("#"+ url).addClass("active");
	}
	
	//fix for modal scrollbar
	$scope.closeLogOpenSign = function(){
		$("#logInModal").modal("hide");
		$("#signUpModal").modal("show");
		$('#logInModal').on('hidden.bs.modal', function () {
    		$('body').addClass('modal-open');
		});
	}
	$scope.closeLogOpenPass = function(){
		$("#logInModal").modal("hide");
		$("#passRecoveryModal").modal("show");
		$('#logInModal').on('hidden.bs.modal', function () {
    		$('body').addClass('modal-open');
		});
	}
	$scope.closeSignOpenLog = function(){
		$("#signUpModal").modal("hide");
		$("#logInModal").modal("show");
		$('#signUpModal').on('hidden.bs.modal', function () {
    		$('body').addClass('modal-open');
		});
	}
	//collapse navbar on click of a link or button
	$(document).on('click','.navbar-collapse.in',function(e) {
		if( $(e.target).is('a:not(".dropdown-toggle")') ) {
			$(this).collapse('hide');
		}
		else if( $(e.target).is('button:not(".dropdown-toggle")') ) {
			$(this).collapse('hide');
		}

	});

	$scope.volunteer = { 
		password:null,
		user:null,
		remember: false
	}
	//arxikopoiisi
	
	$scope.newUser = { name:null,	
					   surname:null,	
					   username:null,	//arxikopoiisi
					   email:null,	
					   birthday: null,
					   password:null,
					   sex:null }	
			

	
 //check if username already exists
	$scope.checkUsername = function(){
		//alert("checkusername");
		console.log($scope.newUser.username);
		$http({
		 method: 'post',              //elegxw an yparxei to username 
		 url:'SignUpController' , //thelw location

		 params: {action:"recogniseUsername",username:$scope.newUser.username}    
		}).then(function successCallback(response) 
		{  
		 //$scope.usernamestatus = 1; // gia elegxo
		 $scope.usernamestatus = response.data;
		 });
	   }
	

//check if email already exists
	$scope.checkEmail = function ()
	{//alert("hiii");


	console.log($scope.newUser.email);

		$http({
			method: 'post',                     //idio gia email
			url: 'SignUpController', //thelw location
			params: {action:"recogniseEmail",email:$scope.newUser.email}
		   }).then(function successCallback(response) 
		   {
			  // $scope.emailstatus=0 ;	//gia check pws leitourgei
			$scope.emailstatus = response.data;
		   });
		}

		//dinei analogo minima an yparxei i oxi
		$scope.alertUsername = function(usernamestatus)
		{
			if(usernamestatus == 0)
			{	
				return 'ng-invalid';	//vgazei to response me kokkino => not available
			}
			else
			{
				return 'hide';	//alliws tipota
			}
		}
		
		$scope.alertEmail = function(emailstatus)
		{
			if(emailstatus == 0)
			{ 
			 	return 'ng-invalid';	//vgazei to response me kokkino => not available
			}
			else
			{
			 return 'hide';	//alliws tipota
			}
		}
	$scope.successSign = false;
	$scope.ageOK = true;
	//move to database after submiting the form	 
	 $scope.add = function()
	 { 
		 var today = new Date()
		 var userage = null;
		 if ($scope.newUser.birthday == null)
			 userage = 0
		 else
		 	userage = today.getFullYear() - $scope.newUser.birthday.getFullYear();
		 console.log(userage)
		 if($scope.newUser.sex=='cat')
            $scope.newUser.sex='female'
        else $scope.newUser.sex='male'
		 if(userage < 18){
			$scope.ageOK = false;
		 	return;
		 }
   		 $http({
    	 	method: 'post',
    	 	url: 'SignUpController' , //thelw location
    		 params: {
				 action:"signup",
				 name:$scope.newUser.name,
				 surname:$scope.newUser.surname,
				 username:$scope.newUser.username,
				 email:$scope.newUser.email,
				 age : userage,
				 password:$scope.newUser.password,
				 sex:$scope.newUser.sex}, 
			}).then(
				function success(response){
					$scope.successSign = true;
					$scope.closeSignOpenLog();
				},
				function error(response){
					console.log(response)
				}
			)             
	}
		   
	//check both email and password for login
	$scope.validUser = true;
	$scope.login = function(){
		var url = "api/login";
		var data = {
			username : $scope.volunteer.user,
			password : $scope.volunteer.password
		};
		//data = $httpParamSerializerJQLike(data);
		data = $httpParamSerializer(data);
		//data = JSON.stringify(data);
		var config = {
			//responseType: "json",
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}
		
		$http.post(url, data, config).then(
			function successCallback(response) {  
				if(response.data.token != ""){
					$("#logInModal").modal("hide");
					console.log(response.data)
					console.log(response.data.token)
					if($scope.volunteer.remember == false){
						$rootScope.setCookie("token", response.data.token, 0);
						$rootScope.setCookie("username", $scope.volunteer.user, 0);
					}
					else {
						$rootScope.setCookie("token", response.data.token, 100);
						$rootScope.setCookie("username", $scope.volunteer.user, 100);
					}
					$rootScope.user = $scope.volunteer.user;
					$rootScope.logged = true;
					$rootScope.token = response.data.token;
				}
				else
					$scope.validUser = false 
			},
			function error(response){
				console.log(response)
			}
		);
	}

	$scope.logout = function(){
		var url = "api/logout";
		var data = {};
		//data = $httpParamSerializerJQLike(data);
		var config = {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'X-OBSERVATORY-AUTH': $rootScope.token.toString()
			}
		}
		data = $httpParamSerializer(data);
		$http.post(url, data, config).then(
			function successCallback(response) {  
				console.log(response.data.message);
				$scope.volunteer = {
					user: "",
					password: "",
					remember: false
				}
				$rootScope.token = "";
				$rootScope.user = "";
				$rootScope.logged = false;
				$rootScope.deleteCookie("username");
				$rootScope.deleteCookie("token");
			}
		)
	}
};
