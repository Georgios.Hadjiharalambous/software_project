angular.module("cavaApp")
.config(function($routeProvider){
	$routeProvider
	.when("/mainPage", {
		templateUrl: "app/components/mainPage/mainPage.html",
		controller: "mainPageCtrl"
	})
	.when("/search", {
		templateUrl: "app/components/searchProduct/searchProduct.html",
		controller: "searchProductCtrl"
	})
	.when("/addProduct", {
		templateUrl: "app/components/addProduct/addProduct.html",
		controller: "addProductCtrl"
	})
	.when("/addShop", {
		templateUrl: "app/components/addShop/addShop.html",
		controller: "addShopCtrl"
	})
	.when("/product/:id", {
		templateUrl: "app/components/productPage/productPage.html",
		controller: "productPageCtrl"
	})
	.when("/shop/:id", {
		templateUrl: "app/components/shopPage/shopPage.html",
		controller: "shopPageCtrl"
	})
	.when("/addPrice", {
		templateUrl: "app/components/addPrice/addPrice.html",
		controller: "addPriceCtrl"
	})
	.when("/error", {
		templateUrl: "app/components/error/error.html",
		controller: "errorCtrl"
	})
	.otherwise({
		templateUrl: "app/components/mainPage/mainPage.html",
		controller: "mainPageCtrl"
	});
});
