angular.module("adminApp", ["ngRoute"]);
angular.module("adminApp")
.controller("adminCtrl", function($location,$scope, $rootScope, $http){
	
	$scope.setLocation = function(url){
		$location.url("/" + url);
	};
	$rootScope.setCookie = function(cname, cvalue, exdays){
		if (exdays != 0){
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+ d.toUTCString();
			document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/observatory";
		}
		else {
			document.cookie = cname + "=" + cvalue + ";path=/observatory";
		}
	}
	$rootScope.getCookie = function(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
		  		c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
		  		return c.substring(name.length, c.length);
			}
		}
		return "";
	}
	$rootScope.deleteCookie = function(cname){
		document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/observatory;";
	}

	$rootScope.user = $scope.getCookie("username");
	$rootScope.token = $scope.getCookie("token")
	
	$rootScope.authUser = function(){
		//check if he is admin
		var url = "../api/login/auth";
		var data = {}
		var config = {
			responseType: "json",
			headers: {
				"X-OBSERVATORY-AUTH" : $rootScope.token
			}
		}
		$http.post(url, data, config).then(
			function success(response){
				console.log(response.data)
				if(!response.data.admin){
					if ($rootScope.user != "")
						alert($rootScope.user + ", δεν επιτρέπεται η πρόσβαση σε αυτή τη σελίδα");
					else 
						alert("Δεν επιτρέπεται η πρόσβαση σε αυτή τη σελίδα");
					var url = document.location.origin + "/observatory";
					window.location.assign(url);
				}
			},
			function failure(response){
				console.log(response.data)
			}
		)
	}

	if($rootScope.user != ""){
		$rootScope.logged = true;
		$rootScope.authUser();
	}
	else $rootScope.logged = false;
	console.log($rootScope.logged);
})
.controller("productsCtrl", productsCtrl)
//.controller("usersCtrl", usersCtrl)
.controller("shopsCtrl", shopsCtrl)
.controller("headerCtrl", headerCtrl);


