var headerCtrl = function($location, $scope, $rootScope, $http, $httpParamSerializer){
	//This is a hack to show active page
	$scope.setLocation = function(url){
		$location.path("/" + url);
		$("#headerNavbar #navList li").removeClass("active");
		$("#"+ url).addClass("active");
	}
	
	//fix for modal scrollbar
	$scope.closeLogOpenSign = function(){
		$("#logInModal").modal("hide");
		$("#signUpModal").modal("show");
		$('#logInModal').on('hidden.bs.modal', function () {
    		$('body').addClass('modal-open');
		});
	}
	$scope.closeLogOpenPass = function(){
		$("#logInModal").modal("hide");
		$("#passRecoveryModal").modal("show");
		$('#logInModal').on('hidden.bs.modal', function () {
    		$('body').addClass('modal-open');
		});
	}
	$scope.closeSignOpenLog = function(){
		$("#signUpModal").modal("hide");
		$("#logInModal").modal("show");
		$('#signUpModal').on('hidden.bs.modal', function () {
    		$('body').addClass('modal-open');
		});
	}
	//collapse navbar on click of a link or button
	$(document).on('click','.navbar-collapse.in',function(e) {
		if( $(e.target).is('a:not(".dropdown-toggle")') ) {
			$(this).collapse('hide');
		}
		else if( $(e.target).is('button:not(".dropdown-toggle")') ) {
			$(this).collapse('hide');
		}

	});

	$scope.admin = { 
		password:null,
		user:null,
	}
			   
	//check both email and password for login
	$scope.validUser = true;
	$scope.login = function(){
		console.log("ok");
		var url = "../api/login";
		var data = {
			username : $scope.admin.user,
			password : $scope.admin.password
		};
		//data = $httpParamSerializerJQLike(data);
		data = $httpParamSerializer(data);
		//data = JSON.stringify(data);
		var config = {
			//responseType: "json";
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			}
		}
		$http.post(url, data, config).then(
			function successCallback(response) {  
				if(response.data.token != ""){
					$("#logInModal").modal("hide");
					console.log("connected");
					$rootScope.setCookie("token", response.data.token, 0);
					$rootScope.setCookie("username", $scope.admin.user, 0);
					$rootScope.user = $scope.admin.user;
					$rootScope.token = response.data.token;
					$rootScope.logged = true;
					
					var url = "../api/login/auth";
					var data = {}
					var config = {
						responseType: "json",
						headers: {
							"X-OBSERVATORY-AUTH" : $rootScope.token
						}
					}
					$http.post(url, data, config).then(
						function success(response){
							console.log(response.data)
							if(!response.data.admin){
								alert($rootScope.user + ", δεν επιτρέπεται η πρόσβαση σε αυτή τη σελίδα");
								var url = document.location.origin + "/observatory";
								window.location.assign(url);
							}
						},
						function failure(response){
							console.log(response.data)
						}
					)
				}
				else
					$scope.validUser = false 
			}
		)               
	}

	$scope.logout = function(){
		var url = "../api/logout";
		var data = {};
		//data = $httpParamSerializerJQLike(data);
		var config = {
			responseType: "json",
			headers: {
				'X-OBSERVATORY-AUTH': $rootScope.token.toString()
			}
		}
		$http.post(url, data, config).then(
			function successCallback(response) {  
				console.log(response.data.message);
				$scope.admin = {
					user: "",
					password: "",
				}
				$rootScope.token = "";
				$rootScope.user = "";
				$rootScope.logged = false;
				$rootScope.deleteCookie("username");
				$rootScope.deleteCookie("token");
				location.assign(location.origin + "/observatory/admin")
			}
		)
	}
};
