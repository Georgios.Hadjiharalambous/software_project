angular.module("adminApp")
.config(function($routeProvider){
	$routeProvider
	.when("/main", {
		templateUrl: "app/components/main/main.html",
	})
	.when("/products", {
		templateUrl: "app/components/products/products.html",
		controller: "productsCtrl"
	})
	.when("/shops", {
		templateUrl: "app/components/shops/shops.html",
		controller: "shopsCtrl"
	})
	/*
	.when("/users", {
		templateUrl: "app/components/users/users.html",
		controller: "usersCtrl"
	})
	*/
	.otherwise({
		templateUrl: "app/components/main/main.html",
	});
});
