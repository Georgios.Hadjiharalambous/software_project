var shopsCtrl = function($location, $scope, $rootScope, $http, $routeParams, $httpParamSerializer){
	$rootScope.authUser();
	document.title = "Διαχειριστής - Καταστήματα";
	$scope.params = {
		start: 0,
		count: 20,
		status: "ALL",
		sort: "id|DESC",
		page: 1
	}

	$scope.selectSet = new Set()

	$scope.changed = function(shop){
		if (shop.check)
			$scope.selectSet.add(shop.id)
		else{
			$scope.selectSet.delete(shop.id);
			shop.edit = false;
		}
	}

	$scope.selectAll = function(){
		for (let shop of $scope.shops){
			$scope.selectSet.add(shop.id)
			shop.check = true
		}
	}

	$scope.clearAll = function(){
		$scope.selectSet = new Set()
		for (let shop of $scope.shops){
			shop.check = false
		}
	}
	
	$scope.newUrl = function(){		
		var url = "/shops?"

		if($scope.params.page!=1){
			$scope.params.start = ($scope.params.page - 1) * $scope.params.count;
			url += "start=" + $scope.params.start + "&";
		}
		/*
		if($scope.params.start!=0)
			url += "start=" + $scope.params.start + "&";
		*/
		if($scope.params.count!=20)
			url += "count=" + $scope.params.count + "&";
		if($scope.params.status!="ALL")
			url += "status=" + $scope.params.status + "&";
		if($scope.params.sort!="id|DESC")
			url += "sort=" + $scope.params.sort + "&";

		return url;
	}

	$scope.changeSort = function(type, dir){
		$scope.params.sort = type + "|" + dir;
		$location.url($scope.newUrl());	
	}
	
	$scope.changePage = function(page){
		//page I want to go starting at 1
		if (page < $scope.lastPage + 1 && page > 0){
			$scope.params.page = page;
			$location.url($scope.newUrl());	
		}
	}

	$scope.viewShop = function(id){
		location.assign(location.origin + "/observatory/#!/shop/" + id)
	}

	$scope.editShop = function(shop){
		if(shop.newName == null){
			shop.newName = shop.name
			shop.newAddress = shop.address
		}
		shop.edit = true;
	}

	$scope.updateShop = function(shop, reload){
		console.log(reload)
		var url="../api/shops/" + shop.id;
		var config = {
			headers : {
				"X-OBSERVATORY-AUTH" : $rootScope.token,
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}
		var data = {
			name : shop.newName,
			address : shop.newAddress
		}
		data = $httpParamSerializer(data);
		$http.patch(url, data, config).then(
			function success(response){
				console.log("success")
				if (reload)
					location.reload();
			},
			function fail(response){
				console.log(response)
			}
		)
	}
	
	$scope.deleteShop = function(id, reload){
		var url="../api/shops/" + id;
		var config = {
			headers : {
				"X-OBSERVATORY-AUTH" : $rootScope.token
			},
			responseType: "json"
		}
		$http.delete(url, config).then(
			function success(response){
				console.log("success")
				if (reload)
					location.reload();
			},
			function fail(response){
				//console.log(response)
			}
		)
	}

	$scope.deleteAll = function(){
		let counter = 1
		for (let id of $scope.selectSet){ 
			if (counter == $scope.selectSet.size)
				$scope.deleteShop(id, true);
			else
				$scope.deleteShop(id, false);
			counter++;
		}
	}
	
	$scope.editAll = function(){
		for( let shop of $scope.shops )
			if ($scope.selectSet.has(shop.id))
				$scope.editShop(shop)
		//for (let id of $scope.selectSet) $scope.editShop(shop);
	}
	
	$scope.updateAll = function(){
		let counter = 1
		for( let shop of $scope.shops ){
			if ($scope.selectSet.has(shop.id)){
				if (counter == $scope.selectSet.size)
					$scope.updateShop(shop, true)
				else
					$scope.updateShop(shop, false)
				counter++
			}
		}
	}

	if($location.path().indexOf("/shops") == 0){

		if($routeParams["start"])
			$scope.params.start = $routeParams["start"];
		if($routeParams["count"])
			$scope.params.count = $routeParams["count"];
		if($routeParams["status"])
			$scope.params.status = $routeParams["status"];
		if($routeParams["sort"])
			$scope.params.sort = $routeParams["sort"];

		
		//var url = "main/products/" + $scope.search.id
		//var url = "app/components/productPage/productExample.json"
		var url="../api/shops?";
		if($scope.params.start!=0)
			url += "start=" + $scope.params.start + "&";
		if($scope.params.count!=20)
			url += "count=" + $scope.params.count + "&";
		if($scope.params.status!="ACTIVE")
			url += "status=" + $scope.params.status + "&";
		if($scope.params.sort!="id|DESC")
			url += "sort=" + $scope.params.sort + "&";
		var config = {
			headers : {
				"X-OBSERVATORY-AUTH" : $rootScope.token
			},
			responseType: "json",
		}
		$http.get(url, config).then(
			function success(response){
				$scope.params.start = response.data.start;
				$scope.params.count = response.data.count;
				$scope.params.total = response.data.total;
				$scope.params.page = $scope.params.start / $scope.params.count + 1;
				$scope.lastPage = Math.floor(response.data.total / $scope.params.count) + 1;
                $scope.shops = response.data.shops;
			},
			function fail(response){
				console.log("Failed to retrieve shop resource");
			}
		);
	}
}
