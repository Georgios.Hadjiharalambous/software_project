var productsCtrl = function($location, $scope, $rootScope, $http, $routeParams, $httpParamSerializer){
	$rootScope.authUser();
	document.title = "Διαχειριστής - Προϊόντα";
	$scope.params = {
		start: 0,
		count: 20,
		status: "ALL",
		sort: "id|DESC",
		page: 1
	}
	
	$scope.selectSet = new Set()

	$scope.changed = function(product){
		if (product.check)
			$scope.selectSet.add(product.id)
		else{
			$scope.selectSet.delete(product.id);
			product.edit=false;
		}
	}
	
	$scope.selectAll = function(){
		for (let product of $scope.products){
			$scope.selectSet.add(product.id)
			product.check = true
		}
	}

	$scope.clearAll = function(){
		$scope.selectSet = new Set()
		for (let product of $scope.products){
			product.check = false
		}
	}

	$scope.newUrl = function(){		
		var url = "/products?"

		if($scope.params.page!=1){
			$scope.params.start = ($scope.params.page - 1) * $scope.params.count;
			url += "start=" + $scope.params.start + "&";
		}
		/*
		if($scope.params.start!=0)
			url += "start=" + $scope.params.start + "&";
		*/
		if($scope.params.count!=20)
			url += "count=" + $scope.params.count + "&";
		if($scope.params.status!="ALL")
			url += "status=" + $scope.params.status + "&";
		if($scope.params.sort!="id|DESC")
			url += "sort=" + $scope.params.sort + "&";

		return url;
	}

	$scope.changeSort = function(type, dir){
		$scope.params.sort = type + "|" + dir;
		$location.url($scope.newUrl());	
	}
	
	$scope.changePage = function(page){
		//page I want to go starting at 1
		if (page < $scope.lastPage + 1 && page > 0){
			$scope.params.page = page;
			$location.url($scope.newUrl());	
		}
	}
	
	$scope.viewProduct = function(id){
		location.assign(location.origin + "/observatory/#!/product/" + id)
	}

	$scope.editProduct = function(product){
		if(product.newName == null){
			product.newName = product.name
			product.newCategory = product.category
			product.newQuantity = product.extraData.quantity_ml
			product.newAlcohol = product.extraData.alcohol
			product.newBarcode = product.extraData.barcode_id
		}
		product.edit = true;
	}

	$scope.updateProduct = function(product, reload){
		var url="../api/products/" + product.id;
		var config = {
			headers : {
				"X-OBSERVATORY-AUTH" : $rootScope.token,
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			responseType: "json"
		}
		var data = {
			name : product.newName,
			category : product.newCategory,
			quantity_ml : product.newQuantity,
			alcohol : product.newAlcohol,
			barcode_id : product.newBarcode
		}
		data = $httpParamSerializer(data);
		$http.patch(url, data, config).then(
			function success(response){
				console.log("success")
				if (reload)
					location.reload();
			},
			function fail(response){
				console.log(response)
			}
		)
	}
	
	$scope.deleteProduct = function(id, reload){
		var url="../api/products/" + id;
		var config = {
			headers : {
				"X-OBSERVATORY-AUTH" : $rootScope.token
			},
			responseType: "json"
		}
		$http.delete(url, config).then(
			function success(response){
				console.log("success")
				if (reload)
					location.reload();
			},
			function fail(response){
				//console.log(response)
			}
		)
	}

	$scope.deleteAll = function(){
		let counter = 1
		for (let id of $scope.selectSet){
			if (counter == $scope.selectSet.size)
				$scope.deleteProduct(id, true);
			else 
				$scope.deleteProduct(id, false);
			counter++
		}
	}
	
	$scope.editAll = function(){
		for( let product of $scope.products )
			if ($scope.selectSet.has(product.id))
				$scope.editProduct(product)
	}
	
	$scope.updateAll = function(){
		let counter = 1
		for( let product of $scope.products )
			if ($scope.selectSet.has(product.id)){
				if (counter == $scope.selectSet.size)
					$scope.updateProduct(product, true)
				else
					$scope.updateProduct(product, false)
			counter++	
			}
	}

	if($location.path().indexOf("/products") == 0){

		if($routeParams["start"])
			$scope.params.start = $routeParams["start"];
		if($routeParams["count"])
			$scope.params.count = $routeParams["count"];
		if($routeParams["status"])
			$scope.params.status = $routeParams["status"];
		if($routeParams["sort"])
			$scope.params.sort = $routeParams["sort"];

		
		//var url = "main/products/" + $scope.search.id
		//var url = "app/components/productPage/productExample.json"
		var url="../api/products?";
		if($scope.params.start!=0)
			url += "start=" + $scope.params.start + "&";
		if($scope.params.count!=20)
			url += "count=" + $scope.params.count + "&";
		if($scope.params.status!="ACTIVE")
			url += "status=" + $scope.params.status + "&";
		if($scope.params.sort!="id|DESC")
			url += "sort=" + $scope.params.sort + "&";
		var config = {
			headers : {
				"X-OBSERVATORY-AUTH" : $rootScope.token
			},
			responseType: "json",
		}
		$http.get(url, config).then(
			function success(response){
				$scope.params.start = response.data.start;
				$scope.params.count = response.data.count;
				$scope.params.total = response.data.total;
				$scope.params.page = $scope.params.start / $scope.params.count + 1;
				$scope.lastPage = Math.floor(response.data.total / $scope.params.count) + 1;
                $scope.products = response.data.products;
			},
			function fail(response){
				console.log("Failed to retrieve product resource");
			}
		);
	}
}
