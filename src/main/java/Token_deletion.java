import java.lang.Thread;
import java.util.concurrent.TimeUnit;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
*This is a class that extends thread class.
*It's function is to run independently 
*from the application and every one day
*i.e 24*60 minutes, runs a procedure
*in our database that deletes all expired
*tokens. Expired means older than 30 days.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/


public class Token_deletion extends Thread {
    public void run() {
    	try{
		    TimeUnit.SECONDS.sleep(10);
		}
		catch(InterruptedException ex)
		{
			System.out.println("caught InterruptedException"+ex);
		}
	    SessionFactory sessionFact = ApplicationC.getSessionFactory();
	    Session session=sessionFact.openSession();
	    session.beginTransaction();
	    Query q=session.createSQLQuery("CALL delete_expired_token()");
	    while(true){
		    q.executeUpdate();
		   	session.getTransaction().commit();
		    session=sessionFact.openSession();
		    session.beginTransaction();
		    q=session.createSQLQuery("CALL delete_expired_token()");

		    try{
				TimeUnit.MINUTES.sleep(24*60);		
			}
			catch(InterruptedException ex)
			{
				System.out.println("caught InterruptedException"+ex);
			}
		}				    	
    }
}   
