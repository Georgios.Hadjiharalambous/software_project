import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import javax.ws.rs.core.Response;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.FormParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import javax.ws.rs.*;
import org.apache.cxf.jaxrs.ext.PATCH;
import javax.ws.rs.core.Response.Status;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.List;
import com.google.gson.*;
import com.google.gson.annotations.Expose;

/**
*This is a class created as a temporary
*entity for database and java connection.
*It's temporary as it is created and 
*destroyed everytime a procedure
*of the database its called.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

public class Temp3 {

	@Expose
	private Long id;
	@Expose
	private String name=null;;
	@Expose
	private String address=null;
	@Expose
	private double lng=0.0;
	@Expose
	private double lat=0.0;
	@Expose
	private Boolean withdrawn=null;	
	private String tags=null;
    @Expose
	private int telephone_number=0;
    @Expose
    private int distance=0;	
	
	
	public Temp3() {}

	public Temp3(Shops shop) {
		this.id=shop.getId();
		this.name=shop.getName();
		this.address=shop.getAddress();
		this.lng=shop.getLng();
		this.lat=shop.getLat();
		String tag=shop.getTags();
		this.withdrawn=shop.getWithdrawn();
		this.telephone_number=shop.getTelephone_number();
	}

	public Long getId() {
		return this.id;
	}
	public void setId(Long shop_id) {
		this.id = shop_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}	
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public Boolean getWithdrawn() {
		return this.withdrawn;
	}
	public void setWithdrawn(Boolean withdrawn) {
		this.withdrawn = withdrawn;
	}
	public String getTags() {
		return this.tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public int getTelephone_number() {
		return telephone_number;
	}
	public void setTelephone_number(int telephone_number) {
		this.telephone_number = telephone_number;
	}
    public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}    
}
