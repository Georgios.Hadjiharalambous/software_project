import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import javax.ws.rs.*;
import org.apache.cxf.jaxrs.ext.PATCH;
import javax.ws.rs.core.Response.Status;
import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gson.*;
import java.util.Arrays;

/**
*This is a class created as a temporary
*entity for database and java connection.
*It's temporary as it is created and 
*destroyed everytime a procedure
*of the database its called.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

public class Temp {

	@Expose
    private Long id;    
	private int distance=-1;		
	
	public Temp (){}
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long product_id) {
		this.id = product_id;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int quantity_ml) {
		this.distance = quantity_ml;
	}
}

