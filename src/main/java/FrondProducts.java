import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import javax.ws.rs.*;
import org.apache.cxf.jaxrs.ext.PATCH;
import javax.ws.rs.core.Response.Status;
import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gson.*;
import java.util.Arrays;
/**
*The FrondProducts class is used for 
*showing the needed data for the 
*GET products rest api calls, as certain attributes 
*need to be shown (as in our product database table
*we have more than the least attributes needed).
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

public class FrondProducts {
	@Expose
    private Long id;
    @Expose
    private String name=null;
    @Expose
    private String description=null;
    @Expose
    private String category=null;
    @Expose
	private List<String> tags=null;
	@Expose
	private Boolean withdrawn=false;
    private Long barcode_id= new Long(0);
	private int quantity_ml=-1;
	private double alcohol=-0.1;
	private int likes=0;	
	@Expose
	private ExtraData extraData;
	
	public FrondProducts (){}

	public FrondProducts (Products product) {
		this.id=product.getId();
		this.name=product.getName();	
		this.extraData=new ExtraData(product.getQuantity_ml(),product.getAlcohol(),product.getBarcode_id(),product.getLikes());
		this.quantity_ml=product.getQuantity_ml();
		this.alcohol=product.getAlcohol();
		this.category=product.getCategory();
		this.description=product.getDescription();	
		String x=product.getTags();
		this.tags = Arrays.asList(x.split(","));
		this.withdrawn=product.getWithdrawn();	
		this.likes=product.getLikes();
	}

	
	public Long getId() {
		return this.id;
	}
	public void setId(Long product_id) {
		this.id = product_id;
	}
	public int getQuantity_ml() {
		return quantity_ml;
	}
	public void setQuantity_ml(int quantity_ml) {
		this.quantity_ml = quantity_ml;
	}
	public double getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(double alcohol) {
		this.alcohol = alcohol;
	}
	public String getCategory() {
		return this.category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<String> getTags() {
		return this.tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public Boolean getWithdrawn() {
		return this.withdrawn;
	}
	public void setWithdrawn(Boolean withdrawn) {
		this.withdrawn = withdrawn;
	}
	public	String getTagsString(){
		if(getTags()==null)
			return null;
		List<String> tag=getTags();
		return String.join(",", tag);
	}
    public Long getBarcode_id(){
    	return this.barcode_id;
    }

    public void setBarcode_id(Long id){
    	this.barcode_id=id;
    }  
    
   	public int getLikes() {
        return likes;
    }
    public void setLikes(int num_likes) {
        this.likes = num_likes;
    }

}

