import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Column;
import java.util.ArrayList;
import java.util.Collection;
import org.hibernate.Query;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.persistence.ElementCollection;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.*;
import com.google.gson.annotations.Expose;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.google.gson.*;
import org.apache.cxf.jaxrs.ext.PATCH;
import  javax.ws.rs.core.Response.Status;

/**
*This class implements the entity Shops
*and the connection between java and the 
*database for this DB table.
*Furthemore it implements, all 
*REST api calls (POST,GET/id,GET(ALL),PATCH/id,PUT/id,DELETE/id).
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

@Entity
@Table(name="shops")
@Path("/shops")

public class Shops {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="shop_id")
	private Long id;
	private String name=null;
	private String address=null;
	private int telephone_number=0;
	private double lng=0.0;	
	private double lat=0.0;
	private String tags=null;
	private Boolean withdrawn=false;	
	
	
	public Shops() {}
	

	public Shops(String name,String address,int num,double lng,double lat,String tags) {
		this.name=name;
		this.address=address;
		this.telephone_number=num;
		this.lng=lng;
		this.lat=lat;
		this.tags=tags;
	}

	public Long getId() {
		return this.id;
	}
	public void setId(Long shop_id) {
		this.id = shop_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public int getTelephone_number() {
		return telephone_number;
	}
	public void setTelephone_number(int telephone_number) {
		this.telephone_number = telephone_number;
	}
	public String getTags() {
		return this.tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public Boolean getWithdrawn() {
		return this.withdrawn;
	}
	public void setWithdrawn(Boolean withdrawn) {
		this.withdrawn = withdrawn;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public Long add(Shops shop ) {
        SessionFactory sessionFact =ApplicationC.getSessionFactory();		
		Session session=sessionFact.getCurrentSession();	
		session.beginTransaction();		
		Long id =(Long)session.save(shop);	
		session.getTransaction().commit();
		return id;
	}

	public Shops(FrondShops shop){
		this.id=shop.getId();
		this.name=shop.getName();
		this.address=shop.getAddress();
		this.lng=shop.getLng();
		this.lat=shop.getLat();
		List<String> tag=shop.getTags();
		this.tags= String.join(",", tag);
		this.withdrawn=shop.getWithdrawn();
		this.telephone_number=shop.getTelephone_number();
	}
   
	/**
	*This methos implements GET/id for shop rest call.
	*It returns the shop object from database with id=id.
	*/	

	@GET
	@Produces("application/json")	
	@Path("{id}")
	public Response getShopById(@PathParam("id") Long id,@QueryParam("format") String format) {//worksss
		if(!("xml".equals(format))){
	    	SessionFactory sessionFact = ApplicationC.getSessionFactory();
			Session session=sessionFact.getCurrentSession();	
			session.beginTransaction();	
			List<Shops> sh=session.createQuery("from Shops where id=:id")
			.setParameter("id",  id).
			list();		
			String result=null;
			for (Shops row : sh) {			
				result=row.toString();
				break;
			}
			if(result!=null){	
			Shops x=sh.get(0);
			FrondShops shop=new FrondShops(x);		 
			session.getTransaction().commit();
			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			return Response.ok(gson.toJson(shop)).build();
			}
			else return Response.status(Response.Status.NOT_FOUND).build(); 
		}else 
 			return Response.status(Response.Status.BAD_REQUEST).build();					
	}

	/**
	*This methos implements GET(ALL) for shops rest call.
	*It returns all the shop objects from database with,with certain sorting.
	*/

 	@GET
	@Produces("application/json")	
	public Response getShops(@QueryParam("start") int start,@QueryParam("count") int count,
	@QueryParam("status") String status,@QueryParam("sort") String sort,@QueryParam("format") String format) {
		if(!("xml".equals(format))){
			if(count<=0) 
				count=20;//default value
			if(start<0)	
				start=0;//default value

			int active=0;//represents if a shop is active or not(withdrawn),default value 1 which means active,if not active = withdrawn
			if("ALL".equals(status))
				active=2;
			else if("WITHDRAWN".equals(status))
				active=1;
			

			String var="shop_id";//order by attribute,default id
			String way_order="DESC";//order by Descending or Ascending,default Descending
			if(sort!=null){
				String[] line = sort.split("\\|");
				if(line[0].equals("id"))
					var="shop_id";
				else var=line[0];
				way_order=line[1];
			}

			String queryOrderWithdrawn="from Shops";//default query
			SessionFactory sessionFact=ApplicationC.getSessionFactory();
			Session session=sessionFact.getCurrentSession();	
			session.beginTransaction();
			String CountQ;//the final query that will be executed for counting the rows needed
			if(active==2)//there is no need for where clause if ALL is selected
				CountQ ="Select count(*) from Shops";
			else  
				CountQ="Select count(*) from Shops where withdrawn="+active;

			Query query=session.createQuery(CountQ);

			Long total=new Long(0);
			total = (Long)query.uniqueResult();//total numbers of rows selected

			if(active==2)
				queryOrderWithdrawn="from Shops order by "+var+" "+way_order;//query without choosing withdrawn and ordering
			else{
				queryOrderWithdrawn="from Shops where withdrawn="+active+
				" order by "+var+" "+way_order;//query with choosing withdrawn and ordering
			}
			Query queryShop=session.createQuery(queryOrderWithdrawn);
			queryShop.setFirstResult(start);//setting the number of row i want to start fetching data
			queryShop.setMaxResults(count);//setting the max number of rows i want to fetch
			List<Shops>  ListShops=queryShop.list();
			List<FrondShops> ListFrondShops=new ArrayList<FrondShops>();
			for(int i=0;i<ListShops.size();i++){//moving products to a new class with all the information frond end needs(eg.extraData)
				FrondShops frondShops=new FrondShops(ListShops.get(i));
				ListFrondShops.add(frondShops);
			}
			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
	        Map<String, Object > LShops = new  LinkedHashMap<>();//Linked hash map is used instead of normal, as linked provides ordering
	        LShops.put("start",start);
	        LShops.put("count",count);
	        LShops.put("total",total);
	        LShops.put("shops",ListFrondShops);
			return Response.ok(gson.toJson(LShops)).build();
		}else 
 			return Response.status(Response.Status.BAD_REQUEST).build();	
	}

	/**
	*This method implements POST REST api call for shop object.
	*If successful returns the object created.
	*The user making this POST call, must have the correct permissions as
	*a "volunteer" user, as named in StRS. 
	*/

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")	
	public Response postShop(@HeaderParam("X-OBSERVATORY-AUTH") String token,
		@FormParam("name") String name,
		@FormParam("address") String address,
		@FormParam("lng") double lng,
		@FormParam("lat") double lat,
		@FormParam("telephone_number") int telephone_number,
		@FormParam("tags") List<String>  tags,
		@QueryParam("format") String format) {//works 
		if(!("xml".equals(format))){
			if(name==null | "".equals(name) | address==null | "".equals(address) | lng==0.0 |lat==0.0 | tags==null)
	    		return Response.status(Response.Status.BAD_REQUEST).build(); 
	    	FrondShops shop=new FrondShops();
	    	shop.setName(name);
	    	shop.setAddress(address);
	    	shop.setLat(lat);
	    	shop.setLng(lng);
	    	shop.setTags(tags);
	    	if(telephone_number>0)
	    		shop.setTelephone_number(telephone_number);
			HashPassword auth=new HashPassword();
            if(!auth.isSimpleUser(token)) 
                return Response.status(Response.Status.FORBIDDEN).build(); 
			SessionFactory sessionFact = ApplicationC.getSessionFactory();
			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create(); 			
	    	Shops Newshop=new Shops(shop);
	   		Long id=Newshop.add(Newshop);
	   		Session session=sessionFact.getCurrentSession();
	   		session.beginTransaction();
	   		Shops shopNew=(Shops)session.get(Shops.class,id);
	   		session.getTransaction().commit();

	   		FrondShops res=new FrondShops(shopNew);
	   	   	return Response.ok(gson.toJson(res)).build();
   	   }else 
 			return Response.status(Response.Status.BAD_REQUEST).build();
 	}

 	/**
	*This method implements PUT/id REST api call for shop object.
	*If successful returns the object updated.
	*The user making this PUT call, must have the correct permissions as
	*a "volunteer" user, as named in StRS. 
	*/

	@PUT
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("{id}")
	public Response putShopById(@HeaderParam("X-OBSERVATORY-AUTH") String token,
		@PathParam("id") Long id,
		@FormParam("name") String name,
		@FormParam("address") String address,
		@FormParam("lng") double lng,
		@FormParam("lat") double lat,
		@FormParam("telephone_number") int telephone_number,
		@FormParam("tags") List<String>  tags,
		@QueryParam("format") String format) {//works 
		if(!("xml".equals(format))){
			if(name==null | "".equals(name) | address==null | "".equals(address) | lng==0.0 |lat==0.0 | tags==null)
	    		return Response.status(Response.Status.BAD_REQUEST).build(); 
			HashPassword auth=new HashPassword();
            if(!auth.isSimpleUser(token)) 
                return Response.status(Response.Status.FORBIDDEN).build(); 					
	    	SessionFactory sessionFact = ApplicationC.getSessionFactory();
			Session session=sessionFact.getCurrentSession();	
			session.beginTransaction();			
			Shops shop_to_update=(Shops)session.get(Shops.class,id);	
			if(shop_to_update==null)
				return Response.status(Response.Status.NOT_FOUND).build(); 

			shop_to_update.setName(name);
			shop_to_update.setAddress(address);
			shop_to_update.setLat(lat);
			shop_to_update.setLng(lng);
			shop_to_update.setTags(String.join(",",tags));
			if(telephone_number>0)
				shop_to_update.setTelephone_number(telephone_number);
			session.update(shop_to_update);
	   		Shops shop_updated=(Shops)session.get(Shops.class,id);
	   		session.getTransaction().commit();
			if(shop_updated!=null){	
				FrondShops fShop=new FrondShops(shop_updated);		 
				Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
				return Response.ok(gson.toJson(fShop)).build();
			}
			else return Response.status(Response.Status.NOT_FOUND).build(); 
		}else 
 			return Response.status(Response.Status.BAD_REQUEST).build();					
	}

	/**
	*This method implements PATCH/id REST api call for shop object.
	*If successful returns the object updated.
	*The user making this PATCH call, must have the correct permissions as
	*a "volunteer" user, as named in StRS. 
	*/

	@PATCH
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")	
	@Path("{id}")
	public Response patchShopById(@HeaderParam("X-OBSERVATORY-AUTH") String token,@PathParam("id") Long id,
		@FormParam("name") String name,
		@FormParam("address") String address,
		@FormParam("lng") double lng,
		@FormParam("lat") double lat,
		@FormParam("telephone_number") int telephone_number,
		@FormParam("tags") List<String>  tags,
		@QueryParam("format") String format) {//works 
		if(!("xml".equals(format))){
			HashPassword auth=new HashPassword();
            if(!auth.isSimpleUser(token)) 
                return Response.status(Response.Status.FORBIDDEN).build(); 			
			SessionFactory sessionFact = ApplicationC.getSessionFactory();
			Session session=sessionFact.getCurrentSession();
			session.beginTransaction();
	   		Shops shop_to_update=(Shops)session.get(Shops.class,id);
			if(name!=null)
				shop_to_update.setName(name);			
			if(lng >0)
				shop_to_update.setLng(lng);
			if(lat >0)
				shop_to_update.setLat(lat);
			if(address!=null)
				shop_to_update.setAddress(address);
			if(tags!=null)
				shop_to_update.setTags(String.join(",",tags));
			if(telephone_number>0)
				shop_to_update.setTelephone_number(telephone_number);
			session.update(shop_to_update);
	   		Shops shop_updated=(Shops)session.get(Shops.class,id);
	   		session.getTransaction().commit();
	   		if(shop_updated!=null){	
				FrondShops fShop=new FrondShops(shop_updated);		 
				Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
				return Response.ok(gson.toJson(fShop)).build();
			}
			else return Response.status(Response.Status.NOT_FOUND).build();
 		}else 
 			return Response.status(Response.Status.BAD_REQUEST).build();

	}

	/**
	*This method implements DELETE/id REST api call for shop object.
	*If successful returns the object updated.
	*The user making this DELETE call, must have the correct permissions as
	*a "volunteer" user, as named in StRS. 
	*If user is a simple user, i.e "volunteer", no deletion occurs instead 
	*the shop is updated as withdrawn=true.
	*Else if the user is "admin" then deletion does occur.
	*/

	@DELETE
	@Consumes("application/json")
	@Produces("application/json") 
	@Path("{id}")
	public Response deleteShopById(@HeaderParam("X-OBSERVATORY-AUTH") String token,@PathParam("id") Long id,@QueryParam("format") String format) {
		if(!("xml".equals(format))){
			HashPassword auth=new HashPassword();
            if(auth.isAdmin(token)){//if user=admin 			
				Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();	
				SessionFactory sessionFact = ApplicationC.getSessionFactory();
				Session session=sessionFact.getCurrentSession();	
				session.beginTransaction();	
				Shops shop	=null;	
				shop=(Shops)session.get(Shops.class,id);	
				if(shop==null)
					return Response.status(Response.Status.NOT_FOUND).build(); 
				try{
					session.delete(shop);
					session.getTransaction().commit();
				} catch (Exception e) {
					Map<String, Object > message = new  LinkedHashMap<>();
		        	message.put("message","Η διαγραφή δεν έγινε, επικοινωνήστε με το διαχειριστή");
	           		return Response.ok(gson.toJson(message)).build();
	        	}
	        	Map<String, Object > message1 = new  LinkedHashMap<>();
		        message1.put("message","OK");
	      		return Response.ok(gson.toJson(message1)).build();	
      		}else{
      			if(!auth.isSimpleUser(token)) 
                	return Response.status(Response.Status.FORBIDDEN).build();
            	else{//if user!=admin
					Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();	
		      		SessionFactory sessionFact = ApplicationC.getSessionFactory();
					Session session=sessionFact.getCurrentSession();	
					session.beginTransaction();			
					Shops shop=(Shops)session.get(Shops.class,id);	
					if(shop==null)
						return Response.status(Response.Status.NOT_FOUND).build(); 
					shop.setWithdrawn(true);
					session.update(shop);
			   		session.getTransaction().commit();
					Map<String, Object > message1 = new  LinkedHashMap<>();
			        message1.put("message","OK");
		      		return Response.ok(gson.toJson(message1)).build();      		
      			}
      		}
		}else 
 			return Response.status(Response.Status.BAD_REQUEST).build();					
	}

}
