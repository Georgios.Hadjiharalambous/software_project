import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.google.gson.*;
/**
*This class implements an entity needed in front end
as a join of product's name and type.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

public class Products_name_types {

	private String type;
	private String name;	
	
	public Products_name_types (){}

	public Products_name_types (String name,String type) {
		this.name=name;		
		this.type=type;			
	}	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
}
