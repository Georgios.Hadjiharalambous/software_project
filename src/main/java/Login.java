import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import javax.ws.rs.FormParam;
import javax.servlet.*;
import javax.ws.rs.ext.ContextResolver;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.*;
import java.util.Random;
import java.security.NoSuchAlgorithmException;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import java.time.LocalDate;
import java.util.Locale;
import java.util.HashMap;
import javax.ws.rs.core.Response.Status;
import org.hibernate.Transaction;
import java.util.LinkedHashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.Query;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Request;
import java.util.ArrayList;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.HeaderParam;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Consumes;
import com.google.gson.annotations.Expose;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.google.gson.*;

/**
*The Login class is used as an end point for 
*the login of the users.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

@Path("/login")
public class Login{



	/**
	*This method is used for creating a token,
	*if the corresponding User_log details(username,password)
	*are correct.
	*Return Response, in which if the User_log is correct
	*a token is attached.
	*/	

	@POST
	public Response createToken(@FormParam("username") String username,@FormParam("password") String password,@QueryParam("format") String format) {
		if(!("xml".equals(format))){
			SessionFactory sessionFact = ApplicationC.getSessionFactory();
			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
	    	if(username==null || password==null || "".equals(username) || "".equals(password)) 	    		
	    		return Response.status(Response.Status.BAD_REQUEST).build(); 
	   		Session session=sessionFact.getCurrentSession();
	   		session.beginTransaction();
	   		int result=-1;
		    String hql = "Select password from Users  where username = :username";
			Query query=session.createQuery(hql)
			.setString("username", username);
			String dbPass="";
			List<Object> res=query.list();
			for (Object row : res) {			
				dbPass=row.toString();
				break;
			}
			if(dbPass!=null){  
				HashPassword hash=new HashPassword();
				String dbPass_temp=null;   
			  	try{
		     		dbPass_temp=hash.hashPassword(password);
		        }
		        catch(NoSuchAlgorithmException e){
		     	   System.out.println(e);
		        }  	
			    if(dbPass.equals(dbPass_temp))
	            	result=1;
				else  
	            	result=0;
	    	} 
	    	else result=0;
	    	String token="";
	    	Boolean tokenExist=false;
	    	if(result==1){
		    	String hql1 = "Select token from UserTokens  where username = :username";
				Query query1=session.createQuery(hql1)
				.setString("username", username);
				List<Object> tok=query1.list();
				String dbToken="";

				for (Object row : tok) {			
					dbToken=row.toString();
					break;
				}
				if(dbToken!=null && !"".equals(dbToken)){       	
			      tokenExist=true;
			      token=dbToken;
		    	}
	    	}	 
	    	if(result==1 && !tokenExist){
		      	session.getTransaction().commit();
		      	byte[] array = new byte[256]; // length is bounded by 7
	   	 		new Random().nextBytes(array);
	    		String generatedString = new String(array, Charset.forName("UTF-8"));
	 			HashPassword hash=new HashPassword();
				token=generatedString;
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());

				try{
	          		token=hash.hashPassword(username+timestamp);
	        	}
	        	catch(NoSuchAlgorithmException e){
	            System.out.println("Υπήρξε κάποιο πρόβλημα με το hashing του token."+e);
	        	}
	        	
				ZoneId zonedId = ZoneId.of( "Europe/Athens" );
				LocalDate today = LocalDate.now( zonedId ); 
	        	UserTokens userToken=new UserTokens(username,token,today);
	        	userToken.add(userToken);
        	}
        	Map<String, Object > tok = new  LinkedHashMap<>();
        	tok.put("token",token);
	   	   	return Response.ok(gson.toJson(tok)).build();
   	   }else {
   	   					System.out.println("user=");

  			return Response.status(Response.Status.BAD_REQUEST).build();}
	}


	/**
	*This method implements rest api call
	*for admin page authentication
	*/
	
	@POST
	@Produces("application/json")	
	@Path("/auth")
	public Response isAdminByToken(@HeaderParam("X-OBSERVATORY-AUTH") String token) {
		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
		Map<String, Object > returnObject = new  LinkedHashMap<>();//Linked hash map is used instead of normal, as linked provides ordering
		HashPassword hash=new HashPassword();
		boolean flagIsAdmin=hash.isAdmin(token);
		returnObject.put("admin",flagIsAdmin);
		boolean flagIsSimple=hash.isSimpleUser(token);
		returnObject.put("user",flagIsSimple);
		return Response.ok(gson.toJson(returnObject)).build();
		
	}


}	

