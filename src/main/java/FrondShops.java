import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import javax.ws.rs.*;
import org.apache.cxf.jaxrs.ext.PATCH;
import javax.ws.rs.core.Response.Status;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.List;
import com.google.gson.*;
import com.google.gson.annotations.Expose;
/**
*The FrondShops class is used for 
*showing the needed data for the 
*GET shops rest api calls, as certain attributes 
*need to be shown (as in our shops database table
*we have more than the least attributes needed).
*Furthermore, it is also used
*to receive the data sent by a POST 
*of a shop.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

public class FrondShops {
	@Expose
	private Long id;
	@Expose
	private String name=null;;
	@Expose
	private String address=null;
	@Expose
	private double lng=0.0;
	@Expose
	private double lat=0.0;
	@Expose
	private Boolean withdrawn=false;
	@Expose
	private List<String> tags=null;
	private int telephone_number=0;	
	
	public FrondShops() {}

	public FrondShops(Shops shop) {
		this.id=shop.getId();
		this.name=shop.getName();
		this.address=shop.getAddress();
		this.lng=shop.getLng();
		this.lat=shop.getLat();
		String tag=shop.getTags();
		this.tags =Arrays.asList(tag.split(","));
		this.withdrawn=shop.getWithdrawn();
		this.telephone_number=shop.getTelephone_number();
	}

	public Long getId() {
		return this.id;
	}
	public void setId(Long shop_id) {
		this.id = shop_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}	
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public Boolean getWithdrawn() {
		return this.withdrawn;
	}
	public void setWithdrawn(Boolean withdrawn) {
		this.withdrawn = withdrawn;
	}
	public List<String> getTags() {
		return this.tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public int getTelephone_number() {
		return telephone_number;
	}
	public void setTelephone_number(int telephone_number) {
		this.telephone_number = telephone_number;
	}
	public	String getTagsString(){
		if(getTags()==null)
			return null;
		List<String> tag=getTags();
		return String.join(",", tag);
	}
    
}
