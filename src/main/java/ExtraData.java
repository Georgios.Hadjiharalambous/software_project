import com.google.gson.annotations.Expose;
/**
*The ExtraData class is used as an entity for 
*the extra data we have on product database class
*and the rest api doesn't need.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

public class ExtraData {
	@Expose
    private int quantity_ml;    
    @Expose
    private double alcohol;
    @Expose
    private Long barcode_id=new Long(0);
    @Expose
    private int likes;

    public int getLikes() {
		return likes;
	}
	public void setLikes(int num_likes) {
		this.likes=num_likes;
	}
    public int getQuantity_ml() {
		return quantity_ml;
	}
	public void setQuantity_ml(int quantity_ml) {
		this.quantity_ml=quantity_ml;
	}
    public double getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(double alcohol) {
		this.alcohol=alcohol;
	}

    public Long getBarcode_id(){
        return this.barcode_id;
    }

    public void setBarcode_id(Long id){
        this.barcode_id=id;
    }    
    
    public ExtraData(int quantity_ml,double alc,Long id,int likes){
        this.quantity_ml=quantity_ml;
        this.alcohol=alc;
        this.barcode_id=id;
        this.likes=likes;
    }
}
