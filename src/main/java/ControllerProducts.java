
import java.util.*; 
import java.util.Date;
import java.util.Iterator; 
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import com.google.gson.*;

/**
*The ControllerProducts class I think
*it might be useless, frondend guys,
*check please whether you use it or 
*not.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/
@WebServlet("/ControllerProducts")
public class ControllerProducts extends HttpServlet {
	
	//global variables for the controller
	SessionFactory sessionFact = ApplicationC.getSessionFactory();

	ZoneId zonedId = ZoneId.of( "Europe/Athens" );
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerProducts() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     	response.setContentType("application/json;charset=UTF-8");
		Session se=sessionFact.getCurrentSession();
		se.beginTransaction();
        PrintWriter out = response.getWriter();
        List<Products> sh=se.createQuery("from Products ").list();	
		Gson gson = new GsonBuilder().setPrettyPrinting().create();           
        List<Products_name_types> Name=new ArrayList<Products_name_types>();
   		Products_name_types x=null;
		for(Products s:sh){    
            x=new Products_name_types(s.getName(),s.getCategory());
            Name.add(x);                 
   		}		
    	out.append(gson.toJson(Name));			
	    se.getTransaction().commit();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
}



