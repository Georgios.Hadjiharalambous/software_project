
import java.time.LocalDate;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.google.gson.*;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
*The Users class is used as an entity for 
*connecting the database and hibernate for the users table.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

@Entity
@Table(name="users")//works-tested
@Path("/users")
public class Users {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long user_id;
	private String name;
    private String surname;
    @Column(unique=true)//needs to be check (front end)
    private String username;
   	private String password;
    private float reputation;
    private int age;
    @Column(unique=true)//needs to be check (front end)
    private String email;
    private boolean sex;
    private Date date_of_registration;    
    
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	 public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public float getReputation() {
		return reputation;
	}
	public void setReputation(float reputation) {
		this.reputation = reputation;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean getSex() {
		return sex;
	}
	public void setSex(boolean sex) {
		this.sex = sex;
	}
	public Date getDate_of_registration() {
		return date_of_registration;
	}
	public void setDate_of_registration(Date date_of_registration) {
		this.date_of_registration = date_of_registration;
	}

	public Users() {}
	
	public Users(String name,String surname,String username,String password,float reputation,int age,String email,boolean sex,LocalDate date) {
		this.name=name;
		this.surname=surname;
		this.username=username;
		this.password=password;
		this.reputation=reputation;
		this.age=age;
		this.email=email;
		this.sex=sex;
		this.date_of_registration=java.sql.Date.valueOf(date);		
	}
	
	public void add(Users user) {
        SessionFactory sessionFact =ApplicationC.getSessionFactory();
		Session session=sessionFact.getCurrentSession();	
		session.beginTransaction();	
		session.save(user);		
		session.getTransaction().commit();	
	}  
}
