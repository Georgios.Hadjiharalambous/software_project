import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.*;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.Query;
import java.util.List;
import java.util.Arrays;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import javax.ws.rs.*;
import org.apache.cxf.jaxrs.ext.PATCH;
import javax.ws.rs.core.Response.Status;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gson.*;
import java.time.LocalDate;

/**
*This is a class created as a temporary
*entity for database and java connection.
*It's temporary as it is created and 
*destroyed everytime a procedure
*of the database its called.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

@Entity
@Table(name="temp1")
public class Temp1 {

	private Double price=0.0;
    private Long productId=new Long(0);
	private Long shopId=new Long(0);
    private String productName=null;
    private String shopName=null;
    private String shopTags=null;
	private String productTags=null;
	private String shopAddress=null;
	private int shopDist=0;	
	private Date date;

	public Temp1 (){}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

    public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopTags() {
		return shopTags;
	}

	public void setShopTags(String shopTags) {
		this.shopTags = shopTags;
	}

	public String getProductTags() {
		return productTags;
	}

	public void setProductTags(String productTags) {
		this.productTags = productTags;
	}

	public String getShopAddress() {
		return shopAddress;
	}

	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	public int getShopDist() {
		return shopDist;
	}

	public void setShopDist(int shopDist) {
		this.shopDist = shopDist;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<String> getTagsList(String tag){
		List<String> myList = new ArrayList<String>(Arrays.asList(tag.split(",")));
		return myList;
	}
	
	public	String getTagsString(String tag){
		if(tag==null)
			return null;
		return String.join(",", tag);
	}
}
