import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.hibernate.Query;
import java.util.Date;
import java.util.Iterator; 
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import com.google.gson.*;

/**
*Where is this used exactly?
*TELL ME IF it is a GET or a POST call PLEASE!!!!
*/


@WebServlet("/shopsController")
public class ShopsController extends HttpServlet {
	
	//global variables for the controller
  	SessionFactory sessionFact ;
	ZoneId zonedId = ZoneId.of( "Europe/Athens" );
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

        public void init(ServletConfig config) throws ServletException{
  		 sessionFact = ApplicationC.getSessionFactory();

	}
    public ShopsController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
	    String ans=request.getParameter("action");
	    if("shops".equals(ans))
	    	getShops(request,response);	    
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
	    String ans=request.getParameter("action");
	    if("shops".equals(ans))
	    	getShops(request,response);	
	}
	protected void getShops(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		Session session=sessionFact.getCurrentSession();
		session.beginTransaction();
		int start=0;
		int count=0;
		if(request.getParameter("start")!=null)
	    	 start=Integer.parseInt(request.getParameter("start"));
		if(request.getParameter("count")!=null)
			 count=Integer.parseInt(request.getParameter("count"));
		if(count<=0) 
			count=20;//default value
		if(start<0)	
			start=0;
		Double lat=0.0;
		Double lng=0.0;
		int distance=0;
		String tags="";
		if(request.getParameter("lng")!=null)
	     lng= Double.parseDouble(request.getParameter("lng"));
		if(request.getParameter("lat")!=null)
	    	 lat= Double.parseDouble(request.getParameter("lat"));
	    if(request.getParameter("dist")!=null)
	    	 distance= Integer.parseInt(request.getParameter("dist"));
	    if(request.getParameter("tags")!=null)
	    	 tags=request.getParameter("tags");
	    tags=tags.replace("[","");
	    tags=tags.replace("]","");
	    String x= ""+'"'; 
	   	tags=tags.replace(x,"");
	    List<String> Ltags=Arrays.asList(tags.split(","));
	  	String shopTag="";
	  	for(int i =0;i<Ltags.size();i++){
	  		String tag=Ltags.get(i);
	  		if(i==0)
	  			shopTag=shopTag+"  s.tags LIKE '%"+tag+"%'  ";
	  		else
	  			shopTag=shopTag+" or   s.tags LIKE '%"+tag+"%'  ";	  		
	  	}
	  	String finalQ="CREATE  TEMPORARY TABLE  IF NOT EXISTS temp2 (select * from shops as s where "+shopTag+" )";
	  	session.createSQLQuery(finalQ).executeUpdate();
		Query q21=session.createSQLQuery("CALL geodist3(?,?,?)")
		.setParameter(0,lng)
		.setParameter(1,lat)
		.setParameter(2,distance); 
		q21.executeUpdate();
	  	String fin=" from Temp3"; 
	  	Query fin1=session.createQuery(fin);
		fin1.setFirstResult(start);//setting the number of row i want to start fetching data
		fin1.setMaxResults(count);//setting the max number of rows i want to fetch
		List<Temp3> ListSearchShop=fin1.list();
		Long total=new Long(0);
		Query queryCount=session.createQuery("Select count(*) from Temp3");
		total =(Long)queryCount.uniqueResult();
		Query q211=session.createSQLQuery("CALL drop_temp2()"); 
		q211.executeUpdate();
		Query q2112=session.createSQLQuery("CALL drop_temp3()"); 
		q2112.executeUpdate();		
		session.getTransaction().commit();
		List<SearchShops> FrondShops=new ArrayList<SearchShops>();
		for(int i=0;i<ListSearchShop.size();i++){
			SearchShops s=new SearchShops(ListSearchShop.get(i));
			FrondShops.add(s);
		}
	    
		Map<String, Object > L = new  LinkedHashMap<>();//Linked hash map is used instead of normal, as linked provides ordering
      	L.put("start",start);
      	L.put("count",count);
      	L.put("total",total);
    	L.put("shops",FrondShops);
    	Gson gson = new GsonBuilder().setPrettyPrinting().create();
    	response.getWriter().append(gson.toJson(L));
	}

	
}



