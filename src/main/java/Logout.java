import java.time.LocalDate;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import javax.ws.rs.FormParam;
import javax.servlet.*;
import javax.ws.rs.ext.ContextResolver;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import java.time.LocalDate;
import java.util.Locale;
import java.util.HashMap;
import javax.ws.rs.core.Response.Status;
import org.hibernate.Transaction;
import java.util.LinkedHashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.Query;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Request;
import java.util.ArrayList;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Consumes;
import com.google.gson.annotations.Expose;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.google.gson.*;

/**
*The Logout class is used as an end point for 
*the exiting of the users.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

@Path("/logout")
public class Logout {

	/**
	*This method "logs out" a user
	*by deleting his valid token from
	*the database if exist.
	*/

	@POST
	public Response auth(@HeaderParam("X-OBSERVATORY-AUTH") String token,@QueryParam("format") String format) {//works 
		if(!("xml".equals(format))){		 	
			SessionFactory sessionFact = ApplicationC.getSessionFactory();
			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
	    	if(token==null || "".equals(token)) 	    		
	    		return Response.status(Response.Status.BAD_REQUEST).build(); 
	   		Session session=sessionFact.getCurrentSession();
	   		session.beginTransaction();
	   		int result=-1;
	   		Query q = session.createQuery("delete from UserTokens where token = :token").
	   		setString("token",token);
			result=q.executeUpdate();	
	      	session.getTransaction().commit();
	      	Map<String, Object > L = new  LinkedHashMap<>();//Linked hash map is used instead of normal, as linked provides ordering
	      	if(result==1)
	      		L.put("message","OK");
	      	else 
	      	   	L.put("message","Υπήρξε πρόβλημα!");
	
	   	   	return Response.ok(gson.toJson(L)).build();
   	   }else 
  			return Response.status(Response.Status.BAD_REQUEST).build();
	}
}	
	

