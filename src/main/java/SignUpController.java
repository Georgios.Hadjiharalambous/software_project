
import java.util.*; 
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Iterator; 
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import org.hibernate.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import com.google.gson.*;

/**
*This class is used as a controller,
*to check the availability of both username and 
*email during a registration of a new user.
*Also if no username or email, exist 
*the new user is added to our database.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

@WebServlet("/SignUpController")
public class SignUpController extends HttpServlet {
	
	//global variables for the controller
	SessionFactory sessionFact = ApplicationC.getSessionFactory();

	ZoneId zonedId = ZoneId.of( "Europe/Athens" );
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUpController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		     	response.setContentType("application/json;charset=UTF-8");	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

	//action parameter is a code word, for the request we want to check/do.
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		if("signup".equals(action))
			addUser(request,response);
		else if("recognise".equals(action))
			isUser(request,response);
		else if( "recogniseUsername".equals(action))
			isUsername(request,response);
		else if("recogniseEmail".equals(action ))
			isEmail(request,response);
		else response.getWriter().append("Something wrong went with 'action' attribute please check!");
	}
	/**
	*This method adds a new user to our database.
	*/

	protected void addUser(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException  {
	 	HashPassword hash=new HashPassword();
		response.setContentType("application/json;charset=UTF-8");
	  	String name = request.getParameter("name");
	   	String username = request.getParameter("username");
	   	String surname = request.getParameter("surname");
	   	Integer age = Integer.parseInt(request.getParameter("age"));
	   	String password = request.getParameter("password");	  
	   	String email = request.getParameter("email");
	   	String sexString = request.getParameter("sex");
		String dbPass=null;
	  	if(name!=null && username!=null && surname!=null && age!=null && password!=null && email!=null && sexString!=null
	  		){
		  	try{
	     		dbPass=hash.hashPassword(password);
	        }
	        	catch(NoSuchAlgorithmException e){
	            System.out.println(e);
	        }		   
	      	LocalDate today = LocalDate.now( zonedId );	    
		   	Boolean sex=false;
		   	if("male".equals(sexString))
			   sex=false;
		   	else if("female".equals(sexString))
			   sex=true;
			Users user=new Users(name,surname,username,dbPass,0f,age,email,sex,today);		    	   
	       	try {
	       		user.add(user);
	       	}
	       	catch(ConstraintViolationException cve){
	       		System.out.print("Something went wrong with the addition of user  : "+cve.getSQLState());
	       	}
       	}else System.out.print("Something went wrong with the dara provided please check again! (null)") ;
    }
	  
	/**
	*This method receives a username and an email and
	*checks whether a user with that email/username exists.
	*It returns as a response, 0 (if user exists) or 1 (if user doesn't exist)
	*/   
  
   	protected void isUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
	  	Session session=sessionFact.getCurrentSession();
		session.beginTransaction();
	   	String username = request.getParameter("username");
	   	String email = request.getParameter("email");
	   	if(username!=null && email!=null){
		   	String hql = "Select user_id from Users  where email = :email or username = :username";
			List<Object> result = session.createQuery(hql)
			.setString("email", email)
			.setString("username", username)
			.list();
	        int num=-1;
	        Gson gson = new GsonBuilder().setPrettyPrinting().create();
			if (result!=null)
	            num=0;
			else
	            num=1;
			response.getWriter().append(gson.toJson(num));	   
   		}else 
   			response.getWriter().append("Υπήρξε κάποιο λάθος με το μέηλ ή όνομα χρήστη παρακαλώ στείλτε τα σωστά (Null)");
   	}
	/**
	*This method receives a username and
	*checks whether a user with that username exists.
	*It returns as a response, 0 (if user exists) or -1 (if user doesn't exist)
	*/ 
   	protected void isUsername(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
	   	Session session=sessionFact.getCurrentSession();
		session.beginTransaction();
	   	String username = request.getParameter("username");
	   	Gson gson = new GsonBuilder().setPrettyPrinting().create();
	   	int num=-1;	
	   	if(username!=null){
	    String hql = "Select user_id from Users  where  username = :username";
		Query query = session.createQuery(hql)
		.setString("username", username);
		String result=null;	
		List<Object> res=query.list();
		for (Object row : res) {			
			result=row.toString();
			break;
		}
		if(result!=null)
			num=Integer.parseInt(result);
			if(num>0)//if user exist,his id is >0,then return 0(exists) else return -1
				num=0;
		}		
		response.getWriter().append(gson.toJson(num));	   
   	}
	/**
	*This method receives an email and
	*checks whether a user with that email exists.
	*It returns as a response, 0 (if user exists) or -1 (if user doesn't exist)
	*/ 

   	protected void isEmail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
	   	Session session=sessionFact.getCurrentSession();
		session.beginTransaction();
	   	String email = request.getParameter("email");
	   	Gson gson = new GsonBuilder().setPrettyPrinting().create();
	   	int num=-1;	
	   	if(email!=null){
	    String hql = "Select user_id from Users  where  email = :email";
		Query query = session.createQuery(hql)
		.setString("email", email);
		String result=null;	
		List<Object> res=query.list();
		for (Object row : res) {			
			result=row.toString();
			break;
		}
		if(result!=null)
			num=Integer.parseInt(result);
			if(num>0)//if user exist,his id is >0,then return 0(exists) else return -1
				num=0;
		}		
		response.getWriter().append(gson.toJson(num));	   
   	}	
}



