import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.hibernate.Query;
import java.util.Date;
import java.util.Iterator; 
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import com.google.gson.*;

/**
*IS this useful???????????
*/


@WebServlet("/PricesController")
public class PricesController extends HttpServlet {
	
	//global variables for the controller
   	SessionFactory sessionFact ;
	ZoneId zonedId = ZoneId.of( "Europe/Athens" );
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

        public void init(ServletConfig config) throws ServletException{
  		 sessionFact = ApplicationC.getSessionFactory();

	}
    public PricesController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		Session session=sessionFact.getCurrentSession();
		session.beginTransaction();
	    Gson gson = new GsonBuilder().setPrettyPrinting().create();     
	    String ans=request.getParameter("action");
        if(ans!=null){
		    String x= ""+'"'; 
            ans=ans.replace(x,"");
	        if("barcodes".equals(ans))
	        	getBarcodes(request,response);
	        else if ("shopsPrice".equals(ans))
	        	getShopsPriceRating(request,response);
        }
        else response.getWriter().append("Πρέπει να εισάγεται action");
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		Session session=sessionFact.getCurrentSession();
		session.beginTransaction();
	    Gson gson = new GsonBuilder().setPrettyPrinting().create();     
	    String ans=request.getParameter("action");
        if(ans!=null){
		    String temp= ""+'"'; 
            ans=ans.replace(temp,"");
	        if("barcodes".equals(ans))
	        	getBarcodes(request,response);
	        else if ("shopsPrice".equals(ans))
	        	getShopsPriceRating(request,response);
        }
        else response.getWriter().append("Πρέπει να εισάγεται action");
	}
	/**
	*This method returns the
	*/
	protected void getShopsPriceRating(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		Session session=sessionFact.getCurrentSession();
		session.beginTransaction();
	    Gson gson = new GsonBuilder().setPrettyPrinting().create();     
		Long id=Long.parseLong(request.getParameter("id"));
		//String hql0="from Products where product_id=:id";
		//List<Products> rs = session.createQuery(hql0)
		//.setParameter("id", id)
		//.list();
		//Products prod=rs.get(0);
		Products prod=(Products)session.get(Products.class,id);

		String hql="Select s.id,s.chain_id,s.name,s.location,s.telephone_number from Shops s,Product_likeness p where p.product_id=:id and s.id=p.shop_id";
		List<Object[]>  result = session.createQuery(hql)
		.setParameter("id", id)
		.list();

		List<Shops_price_rating> temp=new ArrayList<Shops_price_rating>();
		for (Object[] row : result) {
			Shops_price_rating s = new Shops_price_rating();
			s.setShop_id(Long.parseLong(row[0].toString()));
			s.setName(row[2].toString());
			s.setLocation(row[3].toString());
			s.setTelephone_number(Integer.parseInt(row[4].toString()));
			String hql1="Select price,num_likes from  Product_likeness where product_id=:idProduct and shop_id=idShop";
			List<Object[]>  res = session.createQuery(hql1)
			.setParameter("idProduct", id)
			.setParameter("idShop",s.getShop_id())
			.list();
			for (Object[] row1 : res) {
				s.setPrice(Float.parseFloat(row1[0].toString()));
				s.setRating(Integer.parseInt(row1[1].toString()));
				break;
			}
			temp.add(s);
		}

		Products_shops products_shops=new Products_shops(temp,prod);
		String List_product_shops=gson.toJson(products_shops);
		PrintWriter out = response.getWriter();
		out.append(List_product_shops);
	}

	/**
	*This methods receives as input
	*an array of barcodes and
	*returns the products that are associated
	*with these barcodes, if there is any.
	*/
	protected void getBarcodes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Session session=sessionFact.getCurrentSession();
		response.setContentType("application/json;charset=UTF-8");

		session.beginTransaction();
		int start=0;
		int count=0;

		if(request.getParameter("start")!=null)
			start=Integer.parseInt(request.getParameter("start"));
		if(request.getParameter("count")!=null)
			count=Integer.parseInt(request.getParameter("count"));
		Long total=new Long(0);
		if(count<=0) 
			count=20;//default value
		if(start<0)	
			start=0;
		String Lbarcode="0";
		if(request.getParameter("barcodes")!=null || !"".equals(request.getParameter("barcodes")) )
	    	Lbarcode=request.getParameter("barcodes");//mpri na mn xriazete t casting? mpori int anti string?
	   // String StrBarcode=String.join(",",Lbarcode);
	    String StrBarcode=Lbarcode.replace("[","");
	    StrBarcode=StrBarcode.replace("]","");

	    String CountQ="Select count(*) from Products where barcode_id in ("+StrBarcode+")";
	    Query query=session.createQuery(CountQ);//.setParameter(0,StrBarcode);
	    System.out.println(Lbarcode+"edo");


		total = (Long)query.uniqueResult();//total numbers of rows selected
		String hql0="from Products where barcode_id in ("+StrBarcode+")";
		Query queryProd=session.createQuery(hql0);
		queryProd.setFirstResult(start);//setting the number of row i want to start fetching data
		queryProd.setMaxResults(count);//setting the max number of rows i want to fetch
		List<Products> ListProducts =queryProd.list();
		List<FrondProducts> ListFrondProducts=new ArrayList<FrondProducts>();
		for(int i=0;i<ListProducts.size();i++){//moving products to a new class with all the information frond end needs(eg.extraData)
			FrondProducts frondproducts=new FrondProducts(ListProducts.get(i));
			ListFrondProducts.add(frondproducts);
		}



		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();	
		Map<String, Object > Lproducts = new  LinkedHashMap<>();//Linked hash map is used instead of normal, as linked provides ordering
	    Lproducts.put("start",start);
	    Lproducts.put("count",count);
	    Lproducts.put("total",total);
	    Lproducts.put("products",ListFrondProducts);
		response.getWriter().append(gson.toJson(Lproducts));
	}	
}



