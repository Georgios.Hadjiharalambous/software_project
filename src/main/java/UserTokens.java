import java.time.LocalDate;
import java.util.Date;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.google.gson.*;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
*The UserTokens class is used as an entity for 
*connecting the database and hibernate for the userTokens table.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

@Entity
@Table(name="userTokens")
public class UserTokens {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long token_id;
    private String username;
   	private String token;
   	private Date creation_date;

   	public Date getCreation_date(){
   		return this.creation_date;
   	}
   	public void setCreation_date(Date date){
   		this.creation_date=date;
   	}        
	public Long getToken_id() {
		return token_id;
	}
	public void setToken_id(Long token_id) {
		this.token_id = token_id;
	}
	 public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

	public UserTokens() {}

    public UserTokens(String username,String token,LocalDate date){
    this.token=token;
    this.username=username; 
    this.creation_date=java.sql.Date.valueOf(date);   
    }
	
	public void add(UserTokens userTokens) {
        SessionFactory sessionFact =ApplicationC.getSessionFactory();
		Session session=sessionFact.getCurrentSession();	
		session.beginTransaction();	
		session.save(userTokens);		
		session.getTransaction().commit();	
	}	    
}
