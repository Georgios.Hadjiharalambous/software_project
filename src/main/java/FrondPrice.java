import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.format.DateTimeFormatter;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Locale;
import javax.persistence.Column;
import javax.persistence.*;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.Query;
import java.util.List;
import java.util.Arrays;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import javax.ws.rs.*;
import org.apache.cxf.jaxrs.ext.PATCH;
import javax.ws.rs.core.Response.Status;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.*;
import java.time.LocalDate;
/**
*The FrondPrice class is used for 
*showing the needed data for the 
*GET price rest api calls, as certain attributes 
*need to be shown (as in our price table
*we have more than the least needed).
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

public class FrondPrice {
	@Expose
	private Double price=0.0;
    	@Expose
	private String date=null;
	@Expose
    	private String productName=null;
   	 @Expose
    	private Long productId=new Long(0);
    	@Expose
	private List<String> productTags=null;
	@Expose
	private Long shopId=new Long(0);
	@Expose
   	 private String shopName=null;
    	@Expose
 	   private  List<String> shopTags=null;
    	@Expose
	private String shopAddress=null;
	@Expose
	private int shopDist=0;	

	public FrondPrice (){}

	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}

    	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public  List<String> getShopTags() {
		return shopTags;
	}
	public void setShopTags(String shopTags) {
		this.shopTags = getTagsList(shopTags);
	}
	public  List<String> getProductTags() {
		return productTags;
	}
	public void setProductTags(String productTags) {
        
		this.productTags = getTagsList(productTags);
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public int getShopDist() {
		return shopDist;
	}
	public void setShopDist(int shopDist) {
		this.shopDist = shopDist;
	}
	public String getDate() {
      		  return this.date;
	}
	public void setDate(String date) {      
       		 this.date=date;
   	}
   	/**
   	*The two methods above are used as an intermediate
   	*stage of transformation as the Tags 
   	*are saved in the database as a comma 
   	*seperated list. In the front end as an array 
   	*of Strings so we need this transformation.
   	*/
	public List<String> getTagsList(String tag){
		List<String> myList = new ArrayList<String>(Arrays.asList(tag.split(",")));
		return myList;
	}	
	public	String getTagsString(String tag){
		if(tag==null)
			return null;
		return String.join(",", tag);
	}
}
