import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.*;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import javax.ws.rs.core.Response;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import javax.ws.rs.*;
import java.util.Arrays;

import org.apache.cxf.jaxrs.ext.PATCH;
import javax.ws.rs.core.Response.Status;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gson.*;

/**
*This class implements the entity Products
*and the connection between java and the 
*database for this DB table.
*Furthemore it implements, all 
*REST api calls (POST,GET/id,GET(ALL),PATCH/id,PUT/id,DELETE/id).
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

@Entity
@Table(name="products")
@Path("/products")
public class Products {//Works->tested
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="product_id")
    private Long id;
    private String name=null;
    private String description=null;
    private String category=null;
	private String tags=null;
	private Boolean withdrawn=false;
    private Long barcode_id=new Long(0);

	private int quantity_ml=0;
	private double alcohol=0.0;
	private int likes=0;
	
	public Products (){}

	public Products (String name,int quantity_ml,double alcohol,String category,String desc,String tags) {
		this.name=name;
		this.quantity_ml=quantity_ml;
		this.alcohol=alcohol;
		this.category=category;
		this.description=desc;	
		this.tags=tags;
	}
	public int getLikes() {
        return likes;
    }
    public void setLikes(int num_likes) {
        this.likes = num_likes;
    }

	public Long getBarcode_id(){
    return this.barcode_id;
    }

    public void setBarcode_id(Long id){
    this.barcode_id=id;
    }
	public Long getId() {
		return this.id;
	}
	public void setId(Long product_id) {
		this.id = product_id;
	}
	public int getQuantity_ml() {
		return quantity_ml;
	}
	public void setQuantity_ml(int quantity_ml) {
		this.quantity_ml = quantity_ml;
	}
	public String getCategory() {
		return this.category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(double alcohol) {
		this.alcohol = alcohol;
	}
	public String getTags() {
		return this.tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public Boolean getWithdrawn() {
		return this.withdrawn;
	}
	public void setWithdrawn(Boolean withdrawn) {
		this.withdrawn = withdrawn;
	}

	public Long add(Products product) {
        SessionFactory sessionFact = ApplicationC.getSessionFactory();	
		Session session=sessionFact.getCurrentSession();	
		session.beginTransaction();		
		Long id = (Long)session.save(product);
		session.getTransaction().commit();
        return id;
	}

	/**
	*This methos implements GET/id for products rest call.
	*It returns the product object from database with id=id.
	*/	

	@GET
	@Produces("application/json")	
	@Path("{id}")
	public Response getProductById(@PathParam("id") Long id,@QueryParam("format") String format) {
		if(!("xml".equals(format))){
	    	SessionFactory sessionFact = ApplicationC.getSessionFactory();
			Session session=sessionFact.getCurrentSession();	
			session.beginTransaction();	
			List<Products> sh=session.createQuery("from Products where id=:id")
			.setParameter("id",  id).
			list();		
			String result=null;
			for (Products row : sh) {			
				result=row.toString();
				break;
			}
			if(result!=null){	
			Products x=sh.get(0);
			
			FrondProducts pro=new FrondProducts(x);		 
			session.getTransaction().commit();
			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			return Response.ok(gson.toJson(pro)).build();
			}
			else return Response.status(Response.Status.NOT_FOUND).build(); 
		}else 
 			return Response.status(Response.Status.BAD_REQUEST).build();					
	}


	/**
	*This methos implements GET(ALL) for products rest call.
	*It returns all the product objects from database with,with certain sorting.
	*/

	@GET
	@Produces("application/json")	
	public Response getProducts(@QueryParam("start") int start,@QueryParam("count") int count,
	@QueryParam("status") String status,@QueryParam("sort") String sort,@QueryParam("format") String format) {
		if(!("xml".equals(format))){
			if(count<=0) 
				count=20;//default value
			if(start<0)	
				start=0;//default value

			int active=0;//represents if a product is active or not(withdrawn),default value 1 which means active,if not active = withdrawn
			if("ALL".equals(status))
				active=2;
			else if("WITHDRAWN".equals(status))
				active=1;
			

			String var="product_id";//order by attribute,default id
			String way_order="DESC";//order by Descending or Ascending,default Descending
			if(sort!=null){
				String[] line = sort.split("\\|");
				if(line[0].equals("id"))
					var="product_id";
				else var=line[0];
				way_order=line[1];
			}

			String queryOrderWithdrawn="from Products";//default query
			SessionFactory sessionFact=ApplicationC.getSessionFactory();
			Session session=sessionFact.getCurrentSession();	
			session.beginTransaction();
			String CountQ;//the final query that will be executed for counting the rows needed
			if(active==2)//there is no need for where clause if ALL is selected
				CountQ ="Select count(*) from Products";
			else  
				CountQ="Select count(*) from Products where withdrawn="+active;

			Query query=session.createQuery(CountQ);

			Long total=new Long(0);
			total = (Long)query.uniqueResult();//total numbers of rows selected

			if(active==2)
				queryOrderWithdrawn="from Products order by "+var+" "+way_order;//query without choosing withdrawn and ordering
			else{
				queryOrderWithdrawn="from Products where withdrawn="+active+
				" order by "+var+" "+way_order;//query with choosing withdrawn and ordering
			}
			Query queryProd=session.createQuery(queryOrderWithdrawn);
			queryProd.setFirstResult(start);//setting the number of row i want to start fetching data
			queryProd.setMaxResults(count);//setting the max number of rows i want to fetch
			List<Products>  ListProducts=queryProd.list();
			List<FrondProducts> ListFrondProducts=new ArrayList<FrondProducts>();
			for(int i=0;i<ListProducts.size();i++){//moving products to a new class with all the information frond end needs(eg.extraData)
				FrondProducts frondproducts=new FrondProducts(ListProducts.get(i));
				ListFrondProducts.add(frondproducts);
			}
			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();	
		    Map<String, Object > Lproducts = new  LinkedHashMap<>();//Linked hash map is used instead of normal, as linked provides ordering
	        Lproducts.put("start",start);
	        Lproducts.put("count",count);
	        Lproducts.put("total",total);
	        Lproducts.put("products",ListFrondProducts);
			return Response.ok(gson.toJson(Lproducts)).build();
		}else 
 			return Response.status(Response.Status.BAD_REQUEST).build();	
	}

	/**
	*This method implements POST REST api call for product object.
	*If successful returns the object created.
	*The user making this POST call, must have the correct permissions as
	*a "volunteer" user, as named in StRS. 
	*/

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	public Response postProduct(@HeaderParam("X-OBSERVATORY-AUTH") String token,
		@FormParam("description") String description,
		@FormParam("barcode_id") Long barcode_id,
		@FormParam("quantity_ml") int quantity_ml,
		@FormParam("category") String category,
		@FormParam("alcohol") double alcohol,
		@FormParam("name") String name,
		@FormParam("tags") List<String>  tags,
		@QueryParam("format") String format) {
		if(!("xml".equals(format))){
			if(token==null | "".equals(token) | description==null | "".equals(description) |
				category==null | "".equals(category) |
				name==null | "".equals(name) | tags==null )
				return Response.status(Response.Status.BAD_REQUEST).build();
			FrondProducts p=new FrondProducts();
			p.setCategory(category);
			p.setDescription(description);
			p.setName(name);
			p.setTags(tags);
			if(barcode_id!= null && barcode_id!=0)
               	p.setBarcode_id(new Long(barcode_id));
            if(alcohol>0)
            	p.setAlcohol(alcohol);
            if(quantity_ml!=0)
            	p.setQuantity_ml(quantity_ml);

            HashPassword auth=new HashPassword();
            if(!auth.isSimpleUser(token)) 
                return Response.status(Response.Status.FORBIDDEN).build(); 			

			SessionFactory sessionFact = ApplicationC.getSessionFactory();
			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();	
	    	
            
	    	Products pro=new Products();
	    	pro.setName(p.getName());
			pro.setWithdrawn(p.getWithdrawn());
			pro.setCategory(p.getCategory());
			pro.setDescription(p.getDescription());
			pro.setTags(p.getTagsString());
			pro.setAlcohol(p.getAlcohol());
			pro.setQuantity_ml(p.getQuantity_ml());
			pro.setBarcode_id(p.getBarcode_id());
			pro.setLikes(p.getLikes());
			Long id=pro.add(pro);
	   		Session session=sessionFact.getCurrentSession();
	   		session.beginTransaction();
	   		Products prod=(Products)session.get(Products.class,id);
	   		FrondProducts fprod=new FrondProducts(prod);
	   		session.getTransaction().commit();
	   	   	return Response.ok(gson.toJson(fprod)).build();
   	   }else 
 			return Response.status(Response.Status.BAD_REQUEST).build();
 	}

 	/**
	*This method implements PUT/id REST api call for product object.
	*If successful returns the object updated.
	*The user making this PUT call, must have the correct permissions as
	*a "volunteer" user, as named in StRS. 
	*/

	@PUT
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("{id}")
	public Response putProductById(@HeaderParam("X-OBSERVATORY-AUTH") String token,
		@PathParam("id") Long id,
		@FormParam("description") String description,
		@FormParam("barcode_id") Long barcode_id,
		@FormParam("quantity_ml") int quantity_ml,
		@FormParam("category") String category,
		@FormParam("alcohol") double alcohol,
		@FormParam("name") String name,
		@FormParam("tags") List<String>  tags,
		@QueryParam("format") String format) {
		if(!("xml".equals(format))){
			if(token==null | "".equals(token) | description==null | "".equals(description) |
				category==null | "".equals(category) |
				name==null | "".equals(name) | tags==null )
				return Response.status(Response.Status.BAD_REQUEST).build();
			
			HashPassword auth=new HashPassword();
            if(!auth.isSimpleUser(token)) 
                return Response.status(Response.Status.FORBIDDEN).build(); 			
	    	SessionFactory sessionFact = ApplicationC.getSessionFactory();
			Session session=sessionFact.getCurrentSession();	
			session.beginTransaction();			
			Products pro=(Products)session.get(Products.class,id);	
			if(pro==null)
				return Response.status(Response.Status.NOT_FOUND).build(); 

			pro.setCategory(category);
			pro.setDescription(description);
			pro.setName(name);
			pro.setTags(String.join(",",tags));
			if(barcode_id!=0)
               	pro.setBarcode_id(new Long(barcode_id));
            if(alcohol>0)
            	pro.setAlcohol(alcohol);
            if(quantity_ml!=0)
            	pro.setQuantity_ml(quantity_ml);
			//den exo balei na kanei update gia to barcode apefthias mono ean to diloni rita o xristis sto put
			session.update(pro);
	   		Products prod=(Products)session.get(Products.class,id);
	   		session.getTransaction().commit();


			if(prod!=null){	
				FrondProducts fprod=new FrondProducts(prod);		 
				Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();	
				return Response.ok(gson.toJson(fprod)).build();
			}
			else return Response.status(Response.Status.NOT_FOUND).build(); 
		}else 
 			return Response.status(Response.Status.BAD_REQUEST).build();					
	}

	/**
	*This method implements PATCH/id REST api call for product object.
	*If successful returns the object updated.
	*The user making this PATCH call, must have the correct permissions as
	*a "volunteer" user, as named in StRS. 
	*/

 	@PATCH
 	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")	
	@Path("{id}")
	public Response patchProductById(@HeaderParam("X-OBSERVATORY-AUTH") String token,@PathParam("id") Long id,
		@FormParam("description") String description,
		@FormParam("barcode_id") Long barcode_id,
		@FormParam("quantity_ml") int quantity_ml,
		@FormParam("category") String category,
		@FormParam("alcohol") double alcohol,
		@FormParam("name") String name,
		@FormParam("tags") List<String>  tags,
		@QueryParam("format") String format) {
		if(!("xml".equals(format))){			
			HashPassword auth=new HashPassword();
            if(!auth.isSimpleUser(token)) 
                return Response.status(Response.Status.FORBIDDEN).build(); 	
			SessionFactory sessionFact = ApplicationC.getSessionFactory();
			Session session=sessionFact.getCurrentSession();
			session.beginTransaction();
	   		Products pro=(Products)session.get(Products.class,id);
			if(name!=null & !"".equals(name))
				pro.setName(name);
			if(alcohol>0)
				pro.setAlcohol(alcohol);
			if(category!=null & !"".equals(category))
				pro.setCategory(category);
			if(description!=null & !"".equals(description))
				pro.setDescription(description);
			if(quantity_ml>0)
				pro.setQuantity_ml(quantity_ml);
			if(tags!=null & !"".equals(tags))
				pro.setTags(String.join(",",tags));
			if(barcode_id>0)
				pro.setBarcode_id(new Long(barcode_id));
			
			session.update(pro);
	   		Products prod=(Products)session.get(Products.class,id);
	   		session.getTransaction().commit();
	   		if(prod!=null){	
				FrondProducts fprod=new FrondProducts(prod);		 
				Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();	
				return Response.ok(gson.toJson(fprod)).build();
			}
			else return Response.status(Response.Status.NOT_FOUND).build();
 		}else 
 			return Response.status(Response.Status.BAD_REQUEST).build();
	}	

	/**
	*This method implements DELETE/id REST api call for product object.
	*If successful returns the object updated.
	*The user making this DELETE call, must have the correct permissions as
	*a "volunteer" user, as named in StRS. 
	*If user is a simple user, i.e "volunteer", no deletion occurs instead 
	*the product is updated as withdrawn=true.
	*Else if the user is "admin" then deletion does occur.
	*/

	@DELETE
	@Consumes("application/json")
	@Produces("application/json") 
	@Path("{id}")
	public Response deleteProductById(@HeaderParam("X-OBSERVATORY-AUTH") String token,@PathParam("id") Long id,
		@QueryParam("format") String format) {
		if(!("xml".equals(format))){
			HashPassword auth=new HashPassword();
            if(auth.isAdmin(token)){//if user=admin		
				Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();	
				SessionFactory sessionFact = ApplicationC.getSessionFactory();
				Session session=sessionFact.getCurrentSession();	
				session.beginTransaction();			
				Products pro=(Products)session.get(Products.class,id);	
				if(pro==null)
					return Response.status(Response.Status.NOT_FOUND).build(); 
				try{
					session.delete(pro);
					session.getTransaction().commit();
				} catch (Exception e) {
					Map<String, Object > message = new  LinkedHashMap<>();
		        	message.put("message","Η διαγραφή δεν έγινε, επικοινωνήστε με το διαχειριστή");
		       		return Response.ok(gson.toJson(message)).build();
		    	}
		    	Map<String, Object > message1 = new  LinkedHashMap<>();
		        message1.put("message","OK");
		  		return Response.ok(gson.toJson(message1)).build();	
      		}else{
      			if(!auth.isSimpleUser(token)) 
                	return Response.status(Response.Status.FORBIDDEN).build();
            	else{//if user!=admin
					Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();	
		      		SessionFactory sessionFact = ApplicationC.getSessionFactory();
					Session session=sessionFact.getCurrentSession();	
					session.beginTransaction();	
					Products pro=null;		
					 pro=(Products)session.get(Products.class,id);	
					if(pro==null)
						return Response.status(Response.Status.NOT_FOUND).build(); 
					pro.setWithdrawn(true);
					session.update(pro);
			   		session.getTransaction().commit();
					Map<String, Object > message1 = new  LinkedHashMap<>();
			        message1.put("message","OK");
		      		return Response.ok(gson.toJson(message1)).build();
		      	}
		    }  	
		}else 
 			return Response.status(Response.Status.BAD_REQUEST).build();					
	}
	
	
}
