import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.google.gson.*;



public class Shops_price_rating {
	
	private Long shop_id;
	private String name;
	private String location;
	private int telephone_number;
    private int rating;
    private float price;
	
	
	
	public Shops_price_rating() {}

	public Shops_price_rating(String name,String loc,int num) {
		this.name=name;
		this.location=loc;
		this.telephone_number=num;	
	}

	public Long getShop_id() {
		return shop_id;
	}

	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getTelephone_number() {
		return telephone_number;
	}

	public void setTelephone_number(int telephone_number) {
		this.telephone_number = telephone_number;
	}

    public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

    public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
	public void add(Shops_price_rating shops_price_rating ) {
        SessionFactory sessionFact =ApplicationC.getSessionFactory();	
		Session session=sessionFact.getCurrentSession();	
		session.beginTransaction();		
		session.save(shops_price_rating);
		session.getTransaction().commit();
	}
}

