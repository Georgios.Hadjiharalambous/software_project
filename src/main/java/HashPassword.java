import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.*;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import javax.ws.rs.core.Response;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import javax.ws.rs.*;
import org.apache.cxf.jaxrs.ext.PATCH;
import javax.ws.rs.core.Response.Status;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gson.*;

/**
*The HashPassword class is used 
*for generating the corresponding
*hashed password that will be saved
*in the database. 
*Also, this class implements 
*user and admin authentication.
 
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

public class HashPassword {


    /**
    *This method implements password hashing.
    *The algorithm used is SHA.
    *Passwords, are wrong to be
    *saved withoud being hashed first, as
    *in chance of hacking the database, all
    *of the users passwords, will be known.
    *Instead, with hashing the authentication is done
    *and the passwords are safe at the same time.
    */  

   	

    public static String hashPassword(String password) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(password.getBytes());
        byte[] b = md.digest();
        StringBuffer sb = new StringBuffer();
        for(byte b1 : b){
            sb.append(Integer.toHexString(b1 & 0xff).toString());
        }
        return sb.toString();
    }


    /**
    *This method, is used to confirm whether,
    *the password entered from the user,is
    *the same as the one stored in the database
    *for this particular user.
    *Returns true/false.
    */
    public static boolean confirmPassword(String pass,String dbPass){
 			String hashedPassword=null;
    	try{
           hashedPassword=hashPassword(pass);
        }
        catch(NoSuchAlgorithmException e){
            System.out.println(e);
        }
    		
    		if(hashedPassword.equals(dbPass))
    			return true;
    		else return false;
    }

    /**
    *This method, is used to confirm whether,
    *a user token is stored in the database.
    *To be more specific, it used for
    *authentication of a user action.
    *Returns true/false.
    */
    public static Boolean isSimpleUser(String token){
		SessionFactory sessionFact = ApplicationC.getSessionFactory();
		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
    	if(token==null || "".equals(token)) 	    		
    		return false;
   		Session session=sessionFact.getCurrentSession();
   		session.beginTransaction();
   		List<UserTokens> L = session.createQuery("from UserTokens where token = :token").
   		setString("token",token).list();
      	session.getTransaction().commit();
      	String res=null;
    	for(UserTokens row:L){
    		res=row.toString();
    		break;
    	}	
    	Boolean flag=false;
    	if(res!=null)
    		flag=true;
    	else
    		flag=false;
    	return flag;
    }
   
    /**
    *This method, is used to confirm whether,
    *a user token belongs to a user who is Admin.
    *To be more specific, it used for
    *authentication of an user-admin action.
    *Returns true/false.
    */
    public static Boolean isAdmin(String token){
  		SessionFactory sessionFact = ApplicationC.getSessionFactory();
  		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
    	if(token==null || "".equals(token)) 	    		
    		return false;
      	Boolean flag=false;
   		Session session=sessionFact.getCurrentSession();
   		session.beginTransaction();
   		List<UserTokens> LUserTokens = session.createQuery("from UserTokens where token = :token").
   		setString("token",token).list();
    	String res=null;
    	for(UserTokens row:LUserTokens){
    		res=row.toString();
    		break;
    	}
      	if(res!=null){
	        String username=LUserTokens.get(0).getUsername();
	        List<Users> LUsers = session.createQuery("from Users where username = :username").
	        setString("username",username).list();
	        res=null;
	        for(Users row:LUsers){
	          res=row.toString();
	          break;
	        }
	        if(res!=null){
	          Admins admin=null;
	          admin=(Admins) session.get(Admins.class,LUsers.get(0).getUser_id());
	          if(admin!=null){
	            flag=true;
	          }
	        }
      	}
      	session.getTransaction().commit();
     	return flag;
    }
   
}

