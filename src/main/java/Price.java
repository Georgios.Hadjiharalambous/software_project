import java.time.LocalDate;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import javax.ws.rs.FormParam;
import javax.servlet.*;
import javax.ws.rs.ext.ContextResolver;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import java.time.LocalDate;
import java.util.Locale;
import java.util.HashMap;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.Response.Status;
import org.hibernate.Transaction;
import java.util.LinkedHashMap;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.Query;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Request;
import java.util.ArrayList;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Consumes;
import com.google.gson.annotations.Expose;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.google.gson.*;
import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang.time.DateUtils;




/**
*The Price class is used as an entity for 
*connecting the database and hibernate for the price table.
*Furthemore it implements persistence of price object
*and REST api calls, GET/POST price.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

@Entity
@Path("/prices")
@Table(name="price")
public class Price {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long price_id ;
    @Expose
	private Double price=0.0;  
    private java.util.Date date1=null;
    @Expose
    private String date=null;
    @Expose
    private Long productId;
    @Expose
	private Long shopId;    
	private Long user_id=new Long(1);
	private int price_verified=0;       

    public Long getPrice_id() {
		return price_id;
	}
	public void setPrice_id(Long price_id) {
		this.price_id = price_id;
	}
    public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	 public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	 public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
	public int getPrice_verified() {
		return price_verified;
	}
	public void setPrice_verified(int price_verified) {
		this.price_verified = price_verified;
	}	
	
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	 public Date getDate1() {
			return date1;
	}
	public void setDate1(java.util.Date date) {
		this.date1 = date;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	   	this.date=formatter.format(date);
	}
	
	
	public Price(){}

	public Price(Long user_id,Long productId,Long shopId,Double price,LocalDate date,
		LocalDate dateTo,int price_verified) {
		this.price=price;
		this.setDate1(java.sql.Date.valueOf(date));
		this.price_verified=price_verified;		
        this.productId=productId;
        this.user_id=user_id;
        this.shopId=shopId;
	}
	
	
	
	public Long add(Price pro ) {
		Long id=new Long(0);
        SessionFactory sessionFact =ApplicationC.getSessionFactory();
		Session session=sessionFact.getCurrentSession();	
		session.beginTransaction();		
		id =(Long)session.save(pro);		
		session.getTransaction().commit();	
		return id;
	}

	/**
	*This method implements the GET price
	*REST api call. Actually it implements a 
	*product-prices search in respect with 
	*the criteria given.i.e coordinates, distance 
	*from the shop etc.
	*Returns Response, with the results corresponding 
	*to the criteria entered.
	*/

	@GET
	@Produces("application/json")	
	@Consumes("application/x-www-form-urlencoded")
	public Response getPricesByCriteria(@QueryParam("start") int start,@QueryParam("count") int count,
	@QueryParam("status") String status,@QueryParam("sort") List<String> sort,
	@QueryParam("format") String format,@QueryParam("geoDist") int dist,
	@QueryParam("geoLng") Double lng,@QueryParam("geoLat") Double lat,
	@QueryParam("dateFrom") String date_from,@QueryParam("dateTo") String date_to,
	@QueryParam("shops") List<String> Lshops,@QueryParam("products") List<String> Lproducts,
	@QueryParam("tags") List<String> Ltags) {
		if(!("xml".equals(format))){
			String dateQ=null;
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			if(date_from!=null && !"".equals(date_from) && date_to!=null && !"".equals(date_to)){
				Date to=null;
				try {
					to = formatter.parse(date_to);
				} catch (ParseException e) {
	 				return Response.status(Response.Status.BAD_REQUEST).build();	
				}
				Date from=null;
				try {
					from = formatter.parse(date_from);
				} catch (ParseException e) {
					return Response.status(Response.Status.BAD_REQUEST).build();	
				}				
				String date_toForm = formatter.format(to);
				String date_fromForm=formatter.format(from);
				
				dateQ=" and pr.date >='"+date_fromForm+"' and pr.date<='"+date_toForm+"'";
			}else{
				ZoneId zonedId = ZoneId.of( "Europe/Athens" );
				LocalDate today = LocalDate.now( zonedId ); 
				dateQ="  and pr.date <='"+today+"' and pr.date>='"+today+"'";
			}	
			if(count<=0) 
				count=20;//default value
			if(start<0)	
				start=0;//default value
			Boolean flag=false;//shows whether we will query about distance or not
            if(dist==0 |lng ==null | lat==null )
            	flag=true;
			SessionFactory sessionFact = ApplicationC.getSessionFactory();	
			Session session=sessionFact.getCurrentSession();	
			session.beginTransaction();
        	String sql=" CREATE  TEMPORARY TABLE  IF NOT EXISTS temp "+
			 "(Select p.product_id as productId,p.name as productName,p.tags as productTags,s.shop_id as shopId"+
			 ", s.address shopAddress,s.tags as shopTags ,s.name as shopName, pr.price, pr.date as date  "+
			  " from products as p,shops as s,price as pr WHERE p.product_id=pr.product_id  "+
			  " and s.shop_id=pr.shop_id " ;
			  	String shopTag="";
			  	String productTag="";
			  	for(int i =0;i<Ltags.size();i++){
			  		String tag=Ltags.get(i);
			  		if(i==0){
			  			shopTag=shopTag+" and (  s.tags LIKE '%"+tag+"%'  ";
			  			productTag=productTag+" or  p.tags LIKE '%"+tag+"%'   ";
			  		}else{
			  			shopTag=shopTag+" or   s.tags LIKE '%"+tag+"%'  ";
			  			productTag=productTag+" or p.tags LIKE '%"+tag+"%'   ";
			  		}
			  	}if(Ltags.size()>0)
			  		productTag=productTag+")";

			String what_to_order="price";//order by attribute,default id
			String way_order="ASC";//order by Descending or Ascending,default Descending
			String[] sortAr= new String[3];
			sortAr[0]="  price ASC ";
			 sortAr[1]="";
			 sortAr[2]="";
			if(sort!=null){
				for(int i=0;i<sort.size();i++){
					String sortOne=sort.get(i);				
					String[] line = sortOne.split("\\|");
					System.out.println("line+"+line[0]);
					if("geoDist".equals(line[0]))
						what_to_order="distance";
					else 
						what_to_order=line[0];//price/date/geo.dist-radius
					way_order=line[1];//ASC/DESC
					sortAr[i]=what_to_order+"  "+way_order;
				}
			}
			String sortQ="  ORDER BY "+sortAr[0];
			System.out.println("sortQ"+sortQ);
			for(int i=1;i<sort.size();i++)
				sortQ=sortQ+ ", "+sortAr[i];
			String productsQ=null;
			if(Lproducts!=null && !Lproducts.isEmpty()){
				String StrLproducts=String.join(",",Lproducts);
				productsQ=" and p.product_id in ("+StrLproducts+")";
			}else 
			productsQ="";

			String shopsQ=null;
			if(Lshops!=null && !Lshops.isEmpty()){
				String StrLshops=String.join(",", Lshops);
				shopsQ="  and s.shop_id in ("+StrLshops+")  ";
			}else
			shopsQ="";
			
				
			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			String finalSQL=sql+productsQ+shopsQ+dateQ+shopTag+productTag+"  )";
			Query q1=session.createSQLQuery(finalSQL);
			q1.executeUpdate();
			if(!flag){
				Query q211=session.createSQLQuery("CALL geodist2("+lng+","+lat+","+dist+")"); 
				q211.executeUpdate();
			}else{
				Query q211=session.createSQLQuery("CALL geodist()");
				q211.executeUpdate();

			} 
				
			

			String fin="Select * from temp1 "+sortQ;
			Query fin1=session.createSQLQuery(fin);
			fin1.setFirstResult(start);//setting the number of row i want to start fetching data
			fin1.setMaxResults(count);//setting the max number of rows i want to fetch
			List<Object[]> res=fin1.list();
			Long total=new Long(0);
			Query queryCount=session.createQuery("Select count(*) from Temp1");
			total =(Long)queryCount.uniqueResult();

			List<FrondPrice> QueryRes=new ArrayList<FrondPrice>();	

			for(Object[] row:res){
				FrondPrice newFprice=new FrondPrice();
				newFprice.setProductId( Long.parseLong(row[0].toString()));
				newFprice.setProductName(row[1].toString());
				newFprice.setProductTags(row[2].toString());
				newFprice.setShopId(Long.parseLong(row[3].toString()));
				newFprice.setShopAddress(row[4].toString());
				newFprice.setShopTags(row[5].toString());
				newFprice.setShopName(row[6].toString());
				newFprice.setPrice(Double.parseDouble(row[7].toString()));
				int x=(int)(Double.parseDouble(row[9].toString()));
				newFprice.setShopDist(x);
				newFprice.setDate(row[8].toString());				
				QueryRes.add(newFprice);
			}

			Map<String, Object > L = new  LinkedHashMap<>();//Linked hash map is used instead of normal, as linked provides ordering
	      	L.put("start",start);
	      	L.put("count",count);
	      	L.put("total",total);
	    	L.put("prices",QueryRes);
			Query q21=session.createSQLQuery("CALL drop_temp()"); 
			q21.executeUpdate();
			Query q213=session.createSQLQuery("CALL drop_temp1()"); 
			q213.executeUpdate();
			session.getTransaction().commit();
			return Response.ok(gson.toJson(L)).build();
		}else 
 			return Response.status(Response.Status.BAD_REQUEST).build();	
	}

	/**
	*This method implements the POST price REST api call.
	*It first validates if the user has the correct
	*property to do this post, through token validation 
	*and then makes the actual post - save of the data
	*to the database.
	*Returns Response, and if everything was ok
	*the price object that was created as a json object.
	*/

    

	//The method now used to receive the date, is similar to the ones used for products and shops for post(i.e json data)
	//The other methood,the one commented(two lines of comments right above, are using the form posting method
	//i dont know which one is needed, or if there is even difference between them.)
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	public Response postPrice(@HeaderParam("X-OBSERVATORY-AUTH") String token,
		@QueryParam("format") String format,		 
		@FormParam("dateTo") String date_to,
		@FormParam("dateFrom") String date_from,
		@FormParam("productId") String productIdStr,
		@FormParam("price") String price_Str,
		@FormParam("shopId") String shopId){	
		if(!("xml".equals(format))){
			HashPassword auth=new HashPassword();
            if(!auth.isSimpleUser(token)) 
                return Response.status(Response.Status.FORBIDDEN).build(); 
			if(date_to==null | "".equals(date_to) | "".equals(date_from)| date_from==null| productIdStr==null |
			"".equals(productIdStr) |shopId==null | "".equals(shopId) | price_Str==null | "".equals(price_Str))
				return Response.status(Response.Status.BAD_REQUEST).build();	
			Double price_val=Double.parseDouble(price_Str);
			Long productId=Long.parseLong(productIdStr);

			Date to=null;
			Date from=null;
			Price price= new Price();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			if(date_from!=null && !("".equals(date_to)) && date_to!=null && !("".equals(date_from))){
				
				try {
					to = formatter.parse(date_to);
				} catch (ParseException e) {
	 				return Response.status(Response.Status.BAD_REQUEST).build();	
				}
				
				try {
					from = formatter.parse(date_from);
				} catch (ParseException e) {
					return Response.status(Response.Status.BAD_REQUEST).build();
				}
				
				if(!(price_val>0.0))
					return Response.status(Response.Status.BAD_REQUEST).build();	
				
				price.setPrice(price_val);
				price.setShopId(Long.parseLong(shopId));
				price.setProductId(productId);
			}
			long duration=getDifferenceDays(from,to)+1;
			SessionFactory sessionFact = ApplicationC.getSessionFactory();
			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
	   		Session session=sessionFact.getCurrentSession();
	   		session.beginTransaction();
	   		Date now=from;
	   		price.setDate1(now);
	   		List<Price> ListPrice=new ArrayList<Price>();

	   		for(int i=0;i<duration;i++){
	   			Long id=price.add(price);
	   			sessionFact = ApplicationC.getSessionFactory();
	   			session=sessionFact.getCurrentSession();
	   			session.beginTransaction();
	   			Price priceNew=(Price)session.get(Price.class,id);
	   			ListPrice.add(priceNew);
	   			now=addDays(now, 1);
	   			price.setDate1(now);
	   		}

	   		Map<String, Object > Lprices = new  LinkedHashMap<>();//Linked hash map is used instead of normal, as linked provides ordering
	        Lprices.put("start",0);
   	        Lprices.put("count",20);
	        Lprices.put("total",duration);
	        Lprices.put("prices",ListPrice);

	   		session.getTransaction().commit();
	   	   	return Response.ok(gson.toJson(Lprices)).build();
	   	}else
	   		return Response.status(Response.Status.BAD_REQUEST).build();	
	}
	public static Date addDays(Date date, int days){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

	public static long getDifferenceDays(Date d1, Date d2) {
    long diff = d2.getTime() - d1.getTime();
    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	
}
