import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.google.gson.*;

/**
*The Admin class is used as an entity for 
*the connection of the database 
*and the hibernate framework.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

@Entity
@Table(name="admins")
public class Admins {
	@Id
	private Long user_id;

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	
	public Admins(Long id) {
		this.user_id=id;
	}

	public Admins(){}

	/**
	*This method is used for the persistence
	*of an object (i.d Admins) in the database.  
	*/
	public void addAdmin(Admins admin ) {
        SessionFactory sessionFact =ApplicationC.getSessionFactory();
		Session session=sessionFact.getCurrentSession();
		session.beginTransaction();		
		session.save(admin);
		session.getTransaction().commit();		
	}
}
