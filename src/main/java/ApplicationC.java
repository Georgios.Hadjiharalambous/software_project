import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.hibernate.*;
import org.hibernate.cfg.*;

/**
*The CavaApp class is used to create
*the connection between the java application
*and the database.
*This connection should be created 
*only once, as it is "expensive" from time
*perspective to create and thus it is created
*only once during the start/initialization of the
*java application.
* @author  Georgios Hadjiharalambous
* @version 1.0
* @since   2019-01-25
*/

@ApplicationPath("/api") // set the path to REST web services
public class ApplicationC extends Application {

private static final SessionFactory sessionFactory;

    static {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            sessionFactory = new Configuration().configure().buildSessionFactory();
            System.out.println("The Configuration for the database has been made");
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }



}
